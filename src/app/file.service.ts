import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {AppSettings} from './app.setting';

@Injectable()
export class FileService {

  constructor(private _http : Http) { }

  generateImagePathById(id : string) : string {
    if(id == "0" || !id) {
      return "/assets/img/app/no-image.png";
    }
    return AppSettings.GET_IMAGE_BY_ID + "?id=" + id;
  }

  generatePathByIdToken(id : string, token : string) {
    return AppSettings.GET_IMAGE_BY_TOKEN + id + "&authKey=" + token;
  }

  printFile(id: string) {

    window.open(AppSettings.DOWNLOAD_FILE + id, "_blank");

    // this._http.get(AppSettings.DOWNLOAD_FILE + id, {
    //   responseType: ResponseContentType.Blob
    // }).subscribe(
    //   (response) => {
    //     var blob = new Blob([response.blob()], {type: 'application/pdf'});
    //     const blobUrl = URL.createObjectURL(blob);
    //     const iframe = document.createElement('iframe');
    //     iframe.style.display = 'none';
    //     iframe.src = blobUrl;
    //     document.body.appendChild(iframe);
    //     iframe.contentWindow.print();
    //   });
  }

}
