import {ApiUser} from './user/user.model';
import {Http} from '@angular/http';
import {Injectable} from '@angular/core';

@Injectable()
export class AuthService {

  TOKEN_STORAGE = "authKey";

  FIRST_NAME_STORAGE = "firstName";

  LAST_NAME_STORAGE =  "lastName";

  constructor(private _http: Http) {}

  login(token : string, firstName? : string, lastName? : string) {
    localStorage.setItem(ApiUser.TOKEN_STORAGE, token);
    localStorage.setItem(ApiUser.FIRST_NAME_STORAGE, firstName);
    localStorage.setItem(ApiUser.LAST_NAME_STORAGE, lastName);
  }

  logout() {
    localStorage.removeItem(ApiUser.TOKEN_STORAGE);
    localStorage.removeItem(ApiUser.FIRST_NAME_STORAGE);
    localStorage.removeItem(ApiUser.LAST_NAME_STORAGE);
  }

  loggedIn() : boolean {
    let token = localStorage.getItem(ApiUser.TOKEN_STORAGE);
    return token ? true : false;
  }

  getToken() : string {
    return localStorage.getItem(ApiUser.TOKEN_STORAGE);
  }
 }

 export interface AuthResponse extends Response {
   authKey : string;
 }
