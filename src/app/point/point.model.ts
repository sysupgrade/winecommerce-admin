export class PointModel {
  orderId : string;
  productId : string;
  createdAt : string;
  point : number;
  status : string;
}

export class PointResponse{
  model : PointModel;
  models : PointModel[]=[];
  status : string;
  total : number;
}
