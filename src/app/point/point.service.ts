import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../app.setting';
import {PointResponse} from './point.model';

@Injectable()
export class PointService {

  constructor(private _http: Http) {
  }


  getTotalPoint(customerId: string, token: string) : Observable<PointResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append("customerId", customerId);

    return this._http.get(AppSettings.SALES_USER_POINT + "?" + params.toString(), options)
      .map((response) => response.json());
  }

  listUserPointHistory(page : number, pageSize :number, customerId: string, token: string) :Observable<PointResponse>{
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append("customerId", customerId);
    params.append("page", page+'');
    params.append("pageSize", pageSize+'');

    return this._http.get(AppSettings.SALES_USER_POINT_HISTORY + "?" + params.toString(), options)
      .map((response) => response.json());
  }
}
