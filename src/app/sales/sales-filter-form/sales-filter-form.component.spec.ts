import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SalesFilterFormComponent} from './sales-filter-form.component';

describe('SalesFilterFormComponent', () => {
  let component: SalesFilterFormComponent;
  let fixture: ComponentFixture<SalesFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SalesFilterFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
