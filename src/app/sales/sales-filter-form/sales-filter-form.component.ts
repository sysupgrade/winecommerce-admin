import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DropdownItemModel} from '../../toolkit/toolkit.model';
import {FilterSalesModel} from '../sales.model';
import {UserModel, UserResponse} from '../../user/user.model';
import {AppSettings} from '../../app.setting';

@Component({
  selector: 'th-sales-filter-form',
  templateUrl: './sales-filter-form.component.html',
  styleUrls: ['./sales-filter-form.component.scss']
})
export class SalesFilterFormComponent implements OnInit {

  @Input()
  model: UserModel = new UserModel();

  url: string = AppSettings.USER_LIST + '?page=1&pageSize=5&search=:q';

  models: UserModel[] = [];

  total: number = 0;

  statusValue: string = null;

  q: string = null;

  statusText: string = null;

  @Input()
  id: string;

  startDateValue = null;

  endDateValue = null;

  startDate: number;

  endDate: number;

  statusItems: DropdownItemModel[] =
    [
      {
        text: 'All',
        value: null
      }, {
      text: 'Proses',
      value: '3'
    }, {
      text: 'Accept',
      value: '4'
    }, {
      text: 'Finish',
      value: '6'
    }, {
      text: 'Delete',
      value: '0'
    }
    ];

  @Input()
  finishMode: boolean = false;

  @Output()
  filter: EventEmitter<FilterSalesModel> = new EventEmitter<FilterSalesModel>();

  constructor() {
  }

  ngOnInit() {
  }

  handleOnFilter() {
    this.filter.emit({
      status: this.statusValue,
      startDate: this.startDate,
      endDate: this.endDate,
      customer: this.q
    });

  }

  resetFilter() {
    this.q = null;
    this.statusValue = null;
    this.statusText = null;
    this.startDate = null;
    this.endDate = null;
    this.filter.emit(null);
    this.startDateValue = new Date();
    this.endDateValue = new Date();
  }


  handleCustomerDataChange(response: UserResponse) {
    this.models = response.models;
    this.total = response.total;
  }

  handleOnValueChange(event) {
    this.q = event;
  }

  getStartDate(event) {
    let dateStart = Date.parse(event).toString();
    this.startDate = parseInt(dateStart.substring(0, dateStart.length - 3));
  }

  getEndDate(event) {
    let dateStart = Date.parse(event).toString();
    this.endDate = parseInt(dateStart.substring(0, dateStart.length - 3));
  }
}
