import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SalesListComponent} from './sales-list/sales-list.component';
import {SalesViewComponent} from './sales-view/sales-view.component';
import {SalesFilterFormComponent} from './sales-filter-form/sales-filter-form.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {RouterModule} from '@angular/router';
import {DirectivesModule} from '../theme/directives/directives.module';
import {PipesModule} from '../theme/pipes/pipes.module';
import {ToolkitModule} from '../toolkit/toolkit.module';
import {SalesOrderItemListComponent} from './sales-order-item-list/sales-order-item-list.component';
import {FormsModule} from '@angular/forms';

export const routes = [
  {path: '', component: SalesListComponent},
  {path: ':id', component: SalesViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    PipesModule,
    DirectivesModule,
    RouterModule.forChild(routes),
    FormsModule
  ],


  declarations: [SalesListComponent, SalesViewComponent, SalesFilterFormComponent, SalesOrderItemListComponent],
  exports: [SalesListComponent, SalesViewComponent, SalesFilterFormComponent, SalesOrderItemListComponent],
})
export class SalesModule {
}
