import {SalesruleModel} from '../salesrule/salesrule.model';

export class SalesModel {
  id : number;
  paymentMethodId : number;
  shipmentMethodId : number;
  customerId : string;
  customerName : string;
  totalProductCost : number;
  taxAmount : number;
  shipmentCost : number;
  status : string;
  createdAt : number;
  updatedAt : number;
  imageId : number;
  paymentMethod : string;
  shipmentMethod : string;
  telephone : string;
  salutation : string;
  shippingStreetName:string;
  shippingUnitNumber:string;
  shippingCity:string;
  shippingCountry:string;
  shippingPostalCode:string;
  billingStreetName:string;
  billingUnitNumber:string;
  billingCity:string;
  billingCountry:string;
  billingPostalCode:string;
  orderItem : SaleOrderItemModel[]=[];
  rule : SalesruleModel[]=[];

  shippingAddress : string;
  billingAddress: string;
  dateString: string;
  statusText: string;
}


export class SalesResponse {
  model: SalesModel;
  models: SalesModel[] = [];
  total: number;
  status: string;
}

export class SALES_URL {
  public static ROOT = '/pages/sales/';
}

export class FilterSalesModel {
  status: string;
  startDate: number;
  endDate: number;
  customer: string;
}

export class SaleOrderItemModel {
  id: string;
  orderId: string;
  productId: string;
  productAttributeItemId: string;
  quantity: number;
  price: number;
  productName: string;
  createdAt: string;
  updatedAt: string;
  status: string;
}

export class SALES_STATUS {

  public static STATUS_DELETED : string = '0';


  public static STATUS_SEND_TO_PROSES : string = '3';

  public static STATUS_ADMIN_HAS_ACCEPTED_AND_SEND : string = '4';


  public static STATUS_FINISH : string = '6';
}
