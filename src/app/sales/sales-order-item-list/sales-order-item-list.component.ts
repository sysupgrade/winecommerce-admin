import {Component, Input, OnInit} from '@angular/core';
import {SaleOrderItemModel} from '../sales.model';
import {Router} from '@angular/router';
import {PRODUCT_URL} from '../../product/product.model';

@Component({
  selector: 'th-sales-order-item-list',
  templateUrl: './sales-order-item-list.component.html',
  styleUrls: ['./sales-order-item-list.component.scss']
})
export class SalesOrderItemListComponent implements OnInit {

  @Input()
  models: SaleOrderItemModel[] = [];

  constructor(private _router: Router) {
  }

  ngOnInit() {
  }

  getProductId(id: string) {
    this._router.navigate([PRODUCT_URL.ROOT, id])
  }

}
