import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InvoiceFilterComponent} from './invoice-filter/invoice-filter.component';
import {InvoiceViewComponent} from './invoice-view/invoice-view.component';
import {InvoiceListComponent} from './invoice-list/invoice-list.component';
import {DirectivesModule} from '../../theme/directives/directives.module';
import {PipesModule} from '../../theme/pipes/pipes.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {RouterModule} from '@angular/router';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {InvoiceItemListComponent} from './invoice-item-list/invoice-item-list.component';
import {GetInvoiceComponent} from './get-invoice/get-invoice.component';

export const routes = [
  {path: '', component: InvoiceListComponent},
  {path: ':id', component: GetInvoiceComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    PipesModule,
    DirectivesModule,
    RouterModule.forChild(routes),
  ],

  declarations: [InvoiceListComponent, InvoiceViewComponent, InvoiceFilterComponent, InvoiceItemListComponent, GetInvoiceComponent],
  exports: [InvoiceListComponent, InvoiceViewComponent, InvoiceFilterComponent],
})
export class InvoiceModule { }
