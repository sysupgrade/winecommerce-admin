import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SaleOrderItemModel} from '../../sales.model';
import {PRODUCT_URL} from '../../../product/product.model';

@Component({
  selector: 'th-invoice-item-list',
  templateUrl: './invoice-item-list.component.html',
  styleUrls: ['./invoice-item-list.component.scss']
})
export class InvoiceItemListComponent implements OnInit {

  @Input()
  models: SaleOrderItemModel[] = [];

  constructor(private _router: Router) {
  }

  ngOnInit() {
  }

  getProductId(id: string) {
    this._router.navigate([PRODUCT_URL.ROOT, id])
  }

}
