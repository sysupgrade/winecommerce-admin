import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceItemListComponent } from './invoice-item-list.component';

describe('InvoiceItemListComponent', () => {
  let component: InvoiceItemListComponent;
  let fixture: ComponentFixture<InvoiceItemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceItemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
