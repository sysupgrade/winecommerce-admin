import {Component, Input, OnInit} from '@angular/core';
import {DateService} from '../../../date.service';
import {FilterSalesModel, SALES_STATUS, SALES_URL, SalesModel} from '../../sales.model';
import {Router} from '@angular/router';
import {AuthService} from '../../../auth.service';
import {ProductModel} from '../../../product/product.model';
import {ToasterService} from '../../../toaster.service';
import {parsePaymentMethodToText} from '../../../models/payment.model';
import {SalesService} from '../../sales.service';

@Component({
  selector: 'th-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss'],
  providers: [SalesService, AuthService, ToasterService, DateService]
})
export class InvoiceListComponent implements OnInit {

  @Input()
  page: number = 1;

  @Input()
  pageSize: number = 10;

  status: string = SALES_STATUS.STATUS_ADMIN_HAS_ACCEPTED_AND_SEND;

  search: string = null;

  startDate: number = null;

  endDate: number = null;

  models: SalesModel[] = [];

  total: number = 0;

  model: ProductModel = new ProductModel();

  constructor(
    private _salesService: SalesService,
    private _router: Router,
    private _authService: AuthService,
    private _toasterService: ToasterService,
    private _dateService: DateService
  ) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._salesService.list(this.page, this.pageSize, this.status, this.search, this.startDate, this.endDate)
      .subscribe((response) => {
        this.models = response.models;
        this.total = response.total;
      });

  }

  getProductURL(id: string) {
    this._router.navigate(['pages/invoice', id])
  }

  getDate(date: string): string {
    return this._dateService.getDateNumberFormat(date);
  }

  convertPaymentMethodIdToText(paymentMethodId : number) : string {
    return parsePaymentMethodToText(paymentMethodId);
  }

  handleOnFilter(model: FilterSalesModel) {
    this.search = model.customer;
    this.status = model.status;
    this.startDate = model.startDate;
    this.endDate = model.endDate;
    this.list(this.page);
  }
}
