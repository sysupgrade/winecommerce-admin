import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../auth.service';
import {DateService} from '../../../date.service';
import {SalesModel} from '../../sales.model';
import {SalesService} from '../../sales.service';

@Component({
  selector: 'th-get-invoice',
  templateUrl: './get-invoice.component.html',
  styleUrls: ['./get-invoice.component.scss'],
  providers: [SalesService, DateService, AuthService]
})
export class GetInvoiceComponent implements OnInit {

  orderModel: SalesModel = new SalesModel();

  constructor(private _activatedRoute: ActivatedRoute,
              private _orderService: SalesService,
              private _authService: AuthService,
              private _dateService: DateService) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      this._orderService.get(this._authService.getToken(), params['id']).subscribe(response => {
        this.orderModel = response.model;
      })
    });
  }



  getSubTotal(): number {
    let subTotal = 0;
    for (let item of this.orderModel.orderItem) {
      subTotal += (item.price * item.quantity);
    }

    return subTotal;
  }

  getDiscount(): number {
    let discount = 0;
    if (this.orderModel.rule) {
      for (let item of this.orderModel.rule) {
        discount += item.discount;
      }
    }
    if (discount > 100) {
      discount = 100;
    }
    return (discount * this.getSubTotal()) / 100;

  }

  getTotal(): number {
    return (this.getSubTotal() - this.getDiscount()) +
      (parseInt(this.orderModel.taxAmount +'') +
        parseInt(this.orderModel.shipmentCost+''));
  }

  getDate(date) {
    return this._dateService.getDateNumberFormat(date);
  }


  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title></title>
          <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
          <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
          <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
          <style>
          
          .invoice-title h2, .invoice-title h3 {
            display: inline-block;
            }
          
          .table > tbody > tr > .no-line {
            border-top: none;
            }

          .table > thead > tr > .no-line {
            border-bottom: none;
            }

           .table > tbody > tr > .thick-line {
            border-top: 2px solid;
            }
            
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
}
