import {Component, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../auth.service';
import {FileService} from '../../../file.service';
import {ToasterService} from '../../../toaster.service';
import {SalesService} from '../../sales.service';
import {SALES_STATUS, SalesModel} from '../../sales.model';
import {DateService} from '../../../date.service';
import {UserService} from '../../../user/user.service';
import {UserInfoResponse, UserModel, UserResponse} from '../../../user/user.model';

let jsPDF = require('jspdf');
require('jspdf-autotable');


@Component({
  selector: 'th-invoice-view',
  templateUrl: './invoice-view.component.html',
  styleUrls: ['./invoice-view.component.scss'],
  providers: [SalesService, FileService, AuthService, ToasterService, DateService, UserService]
})
export class InvoiceViewComponent implements OnInit {

  orderId: string;

  @Output()
  model: SalesModel;

  dialogConfirm: boolean = false;

  selectedStatus: string;

  dialogDelete : boolean = false;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _salesService: SalesService,
    private _fileService: FileService,
    private _authService: AuthService,
    private _toasterService: ToasterService,
    private _router : Router,
    private _dateService : DateService,
    private _userService : UserService
  ) {
  }

  ngOnInit() {
    this.model = new SalesModel();
    this._activatedRoute.params.subscribe(params => {
      this.orderId = params['id'];
      this.getData();
    });
  }

  getShippingAddress (response : UserResponse) : string{
    return response.model.shippingStreetName + ', ' +
    response.model.shippingUnitNumber + ', ' +
    response.model.shippingCity + ', ' +
    response.model.shippingCountry + ', ' +
    response.model.shippingPostalCode;
  }

  getBillingAddress (response : UserResponse) : string{
    return response.model.billingStreetName + ', ' +
    response.model.billingUnitNumber + ', ' +
    response.model.billingCity + ', ' +
    response.model.billingCountry + ', ' +
    response.model.billingPostalCode;
  }

  getData() {
    this._salesService.get(this._authService.getToken(), this.orderId)
      .subscribe((response) => {
        this._userService.get(response.model.customerId, this._authService.getToken()).subscribe(userResponse=>{
          response.model.shippingAddress = this.getShippingAddress(userResponse);
          if (this.getShippingAddress(userResponse) !== this.getBillingAddress(userResponse)){
            response.model.billingAddress = this.getBillingAddress(userResponse);
          }
        });
        response.model.dateString = this.getStringDate(response.model.updatedAt);
        this.model = response.model;
      });
  }


  handlePrintOut() {

    let headerCustomer = [
      {title : 'NAME', dataKey :'customerName'},
      {title : 'DATE', dataKey :'dateString'},
      {title : 'ADDRESS', dataKey: 'shippingAddress'},
      {title : 'BILLING ADDRESS', dataKey: 'billingAddress'}
    ];

    let dataCustomer : SalesModel[]=[];
    dataCustomer.push(this.model);

    let header = [
      {title: 'PRODUCT NAME', dataKey: 'productName'},
      {title: 'QUANTITY', dataKey: 'quantity'},
      {title: 'PRICE', dataKey: 'price'},
    ];
    let data = this.model.orderItem;
    let doc = new jsPDF('p', 'pt');

    doc.autoTable(headerCustomer, dataCustomer);

    doc.autoTable(header, data, {

      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {},
      bodyStyles: {},
      alternateRowStyles: {},
      columnStyles: {},

      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: [120,40,40,40], // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number,
      showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,

      // Hooks
      createdHeaderCell: function (cell, data) {

      },
      createdCell: function (cell, data) {
      },
      drawHeaderRow: function (row, data) {
      },
      drawRow: function (row, data) {
      },
      drawHeaderCell: function (cell, data) {
      },
      drawCell: function (cell, data) {
      },
      addPageContent: function (data) {
        // doc.text(head, 40, 30);
      }
    });

    doc.autoPrint();

    window.open(doc.output('bloburl'), '_blank');
  }

  changeStatus() {
    this._salesService.changeStatus(this.model.id, this.selectedStatus, this._authService.getToken())
      .subscribe((response) => {
        if (response.status)
          this.getData();
        this._toasterService.openToast('SUCCESS', '', 'success');
      });
  }


  deleteOrder(){
    this._salesService.delete(this.orderId, this._authService.getToken()).subscribe(response=>{
      if(response.status){
        this._router.navigate(['/pages/invoice']);
      }
    });
  }

  setDeleteDialog(val : boolean){
    this.dialogDelete = val;
  }

  getStringDate(date) : string{
    return this._dateService.getDateNumberFormat(date);
  }

  getUserInfo(userId : string) : UserModel{
    this._userService.get(userId, this._authService.getToken()).subscribe(response=>{
      return response;
    });
    return null;
  }
}
