import {Component, Input, OnInit} from '@angular/core';
import {FilterProductModel, ProductModel} from '../../product/product.model';
import {DateService} from '../../date.service';
import {AuthService} from '../../auth.service';
import {Router} from '@angular/router';
import {ToasterService} from '../../toaster.service';
import {SalesService} from '../sales.service';
import {FilterSalesModel, SALES_STATUS, SALES_URL, SalesModel} from '../sales.model';
import { parsePaymentMethodToText } from '../../models/payment.model';

@Component({
  selector: 'th-sales-list',
  templateUrl: './sales-list.component.html',
  styleUrls: ['./sales-list.component.scss'],
  providers: [SalesService, AuthService, ToasterService, DateService]
})
export class SalesListComponent implements OnInit {

  @Input()
  page: number = 1;

  @Input()
  pageSize: number = 10;

  status: string = null;

  search: string = null;

  startDate: number = null;

  endDate: number = null;

  models: SalesModel[] = [];

  total: number = 0;

  model: ProductModel = new ProductModel();


  dialogConfirm: boolean = false;

  selectedStatus: string;

  selectedSalesModel: SalesModel;

  constructor(
    private _salesService: SalesService,
    private _router: Router,
    private _authService: AuthService,
    private _toasterService: ToasterService,
    private _dateService: DateService
  ) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._salesService.list(this.page, this.pageSize, this.status, this.search, this.startDate, this.endDate)
      .subscribe((response) => {
        this.models = response.models;
        this.total = response.total;
      });

  }

  getProductURL(id: string) {
    this._router.navigate([SALES_URL.ROOT, id])
  }

  handleOnFilter(model) {
    if (model) {
      this.search = model.customer;
      this.status = model.status;
      this.startDate = model.startDate;
      this.endDate = model.endDate;
    }else {
      this.search = null;
      this.status = null;
      this.startDate = null;
      this.endDate = null;
    }
    this.list(this.page);
  }

  getDate(date: string): string {
    return this._dateService.getDateNumberFormat(date);
  }

  convertPaymentMethodIdToText(paymentMethodId : number) : string {
    return parsePaymentMethodToText(paymentMethodId);
  }


  changeStatus() {
    this._salesService.changeStatus(this.selectedSalesModel.id, this.selectedStatus, this._authService.getToken())
      .subscribe((response) => {
        if (response.status) {
          if (this.selectedStatus == '0') {
            this._toasterService.openToast('SUCCESS', 'Delete', 'warning');
          } else {
            this._toasterService.openToast('SUCCESS', '', 'success');
          }
          this._router.navigate(['/pages/sales']);
        }
        this.list(this.page);
      });
  }

  openDialogConfirm(val: boolean, selectedStatus: string, sales: SalesModel) {
    this.selectedStatus = selectedStatus;
    this.selectedSalesModel = sales;
    this.dialogConfirm = val;
  }

  getUserURL(userId : string){
    this._router.navigate(['pages/user', userId]);
  }
}
