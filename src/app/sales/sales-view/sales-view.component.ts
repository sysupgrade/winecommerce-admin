import {Component, OnInit, Output} from '@angular/core';
import {SalesService} from '../sales.service';
import {SALES_STATUS, SalesModel} from '../sales.model';
import {ActivatedRoute, Router} from '@angular/router';
import {FileService} from '../../file.service';
import {AuthService} from '../../auth.service';
import {ToasterService} from '../../toaster.service';

let jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'th-sales-view',
  templateUrl: './sales-view.component.html',
  styleUrls: ['./sales-view.component.scss'],
  providers: [SalesService, FileService, AuthService, ToasterService]
})
export class SalesViewComponent implements OnInit {


  orderId: string;

  @Output()
  model: SalesModel;

  statusDeleted: string = SALES_STATUS.STATUS_DELETED;

  statusSendToProses : string  = SALES_STATUS.STATUS_SEND_TO_PROSES;

  statusAdminHasAcceptedAndSend: string = SALES_STATUS.STATUS_ADMIN_HAS_ACCEPTED_AND_SEND;

  statusFinish: string = SALES_STATUS.STATUS_FINISH;


  dialogConfirm: boolean = false;

  selectedStatus: string;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _salesService: SalesService,
    private _fileService: FileService,
    private _authService: AuthService,
    private _toasterService: ToasterService,
    private _router : Router
  ) {
  }

  ngOnInit() {
    this.model = new SalesModel();
    this._activatedRoute.params.subscribe(params => {
      this.orderId = params['id'];
      this.getData();
    });
  }

  getData() {
    this._salesService.get(this._authService.getToken(), this.orderId)
      .subscribe((response) => {
        this.model = response.model;
      });
  }


  handlePrintOut() {

    let headerCustomer = [
      {title : 'NAME', dataKey :'customerName'},
      {title : 'DATE', dataKey :'dateString'},
      {title : 'ADDRESS', dataKey: 'address'}
    ];

    let dataCustomer : SalesModel[]=[];
    dataCustomer.push(this.model);

    let header = [
      {title: 'PRODUCT NAME', dataKey: 'productName'},
      {title: 'QUANTITY', dataKey: 'quantity'},
      {title: 'PRICE', dataKey: 'price'},
    ];
    let data = this.model.orderItem;
    let doc = new jsPDF('p', 'pt');

    doc.autoTable(headerCustomer, dataCustomer);

    doc.autoTable(header, data, {

      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {},
      bodyStyles: {},
      alternateRowStyles: {},
      columnStyles: {},

      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: [120,40,40,40], // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number,
      showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,

      // Hooks
      createdHeaderCell: function (cell, data) {

      },
      createdCell: function (cell, data) {
      },
      drawHeaderRow: function (row, data) {
      },
      drawRow: function (row, data) {
      },
      drawHeaderCell: function (cell, data) {
      },
      drawCell: function (cell, data) {
      },
      addPageContent: function (data) {
        // doc.text(head, 40, 30);
      }
    });

    doc.autoPrint();

    window.open(doc.output('bloburl'), '_blank');
  }

  getFilePath(imageId) {
    return this._fileService.generateImagePathById(imageId);
  }

  changeStatus() {
    this._salesService.changeStatus(this.model.id, this.selectedStatus, this._authService.getToken())
      .subscribe((response) => {
        if (response.status) {
          if (this.selectedStatus == '0') {
            this._toasterService.openToast('SUCCESS', 'Delete', 'warning');
          } else {
            this._toasterService.openToast('SUCCESS', 'Approve', 'success');
          }
          this._router.navigate(['/pages/sales']);
        }
      });
  }

  openDialogConfirm(val: boolean, selectedStatus: string) {
    this.selectedStatus = selectedStatus;
    this.dialogConfirm = val;
  }

  getWizardActiveStatus(status) {
    if (this.model.status == status) {
      return 'active';
    }
  }


}
