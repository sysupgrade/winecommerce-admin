import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../app.setting';
import {SalesResponse} from './sales.model';

@Injectable()
export class SalesService {

  constructor(private _http: Http) {
  }

  list(page: number, pageSize: number, status: string, customerId: string, startDate: number, endDate: number): Observable<SalesResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');
    if (status) params.append('status', status + "");
    if (customerId) params.append('customerId', customerId);
    if (startDate) params.append('startDate', startDate+'');
    if (endDate) params.append('endDate', endDate+'');

    return this._http.get(AppSettings.SALES_ORDER_LIST + '?' + params.toString(), options)
      .map((response) => response.json());
  }


  changeStatus(orderId: number, status: string, token: string): Observable<SalesResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', orderId + "");
    params.append('status', status);

    return this._http.post(AppSettings.SALES_CHANGE_ORDER_STATUS, params, options)
      .map((response) => response.json());
  }

  get(token : string, orderId: string): Observable<SalesResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '+token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', orderId);

    return this._http.get(AppSettings.SALES_ORDER_GET + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  delete(orderId: string, token : string): Observable<SalesResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '+token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('orderId', orderId);

    return this._http.post(AppSettings.SALES_ORDER_REMOVE , params, options)
      .map((response) => response.json());
  }

  listOrderByUser(token : string, page: number, pageSize: number, customerId: string): Observable<SalesResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '+ token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');
    if (customerId) params.append('customerId', customerId);

    return this._http.get(AppSettings.SALES_ORDER_LIST_BY_USER + '?' + params.toString(), options)
      .map((response) => response.json());
  }

}
