import {Component, OnInit} from '@angular/core';
import {DateService} from '../../../date.service';
import {ReportOrderModel, ReportOrderResponse} from '../../report.model';
import {ReportService} from '../../report.service';
import {AuthService} from '../../../auth.service';
import {parsePaymentMethodToText} from '../../../models/payment.model';

let jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'th-month-report',
  templateUrl: './month-report.component.html',
  styleUrls: ['./month-report.component.scss'],

})
export class MonthReportComponent implements OnInit {

  models: ReportOrderModel[] = [];

  fileType: string;

  date : string;

  constructor(private _authService: AuthService, private _reportService: ReportService, private _dateService: DateService) {
  }

  ngOnInit() {
  }

  print(date, fileType) {
    this.date = date.split('-').reverse().join('-');
    this.fileType = fileType;
    this._reportService.reportByMonth(date, this._authService.getToken()).subscribe(this.handlePrint.bind(this));
  }

  handlePrint(response: ReportOrderResponse) {
    let customerId: string = null;
    let date = this.date;


    for (let model of response.models) {
      model.stringDate = this.getDate(model.updatedAt);
      model.paymentMethod = this.convertPaymentMethodIdToText(parseInt(model.paymentMethodId));
    }

    let header = [
      {title: '#ORDER ID', dataKey: 'id'},
      // {title: 'CUSTOMER NAME', dataKey: 'customerName'},
      {title: 'PAYMENT', dataKey: 'paymentMethod'},
      {title: 'COST', dataKey: 'totalProductCost'},
      // {title: 'TAX', dataKey: 'tax'},
      {title: 'DATE', dataKey: 'stringDate'},
    ];
    let data = response.models;
    let doc = new jsPDF('p', 'pt');
    doc.page=1;

    doc.autoTable(header, data, {
      html: '#table_wrapper',

      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {},
      bodyStyles: {},
      alternateRowStyles: {},
      columnStyles: {},

      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: [40], // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number,
      showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,

      // Hooks
      createdHeaderCell: function (cell, data) {
        doc.setFontSize(12);
        doc.text('REPORT MONTH '+date, 40, 30);
      },
      createdCell: function (cell, data) {
      },
      drawHeaderRow: function (row, data) {
      },
      drawRow: function (row, data) {

        // Colspan
        doc.setFontStyle('bold');
        doc.setFontSize(10);


        if (row.raw.customerId !== customerId) {
          customerId = row.raw.customerId;
          doc.setTextColor('#344154');

          doc.rect(data.settings.margin.left, row.y, data.table.width, 20, 'S');
          doc.autoTableText(row.raw.customerName.toUpperCase() + ' ( ' + parseFloat(row.raw.totalCost).toFixed(2) + ' ) ', data.settings.margin.left + data.table.width / 2, row.y + row.height / 2, {
            halign: 'center',
            valign: 'middle'
          });
          data.cursor.y += 20;
        }
      },
      drawHeaderCell: function (cell, data) {

      },
      drawCell: function (cell, data) {
      },

      addPageContent: function (data) {
        doc.setFontStyle('italic');
        doc.setFontSize(12);
        doc.text('page ' + doc.page +' | '+ date, data.settings.margin.left, 810);
        doc.page ++;
      }
    });

    doc.autoPrint();

    if (this.fileType === 'pdf') {
      doc.save();
    }
    else {
      window.open(doc.output('bloburl'), '_blank');
    }

    console.log(doc.autoTable);
  }

  getDate(date: string): string {
    return this._dateService.getDateNumberFormat(date);
  }

  convertPaymentMethodIdToText(paymentMethodId: number): string {
    return parsePaymentMethodToText(paymentMethodId);
  }
}
