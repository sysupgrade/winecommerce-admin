import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WeekReportComponent} from './week-report.component';

describe('WeekReportComponent', () => {
  let component: WeekReportComponent;
  let fixture: ComponentFixture<WeekReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WeekReportComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
