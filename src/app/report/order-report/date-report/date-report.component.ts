import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../auth.service';
import {ReportService} from '../../report.service';
import {ReportOrderModel, ReportOrderResponse} from '../../report.model';
import {DateService} from '../../../date.service';
import {parsePaymentMethodToText} from '../../../models/payment.model';

let jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'th-date-report',
  templateUrl: './date-report.component.html',
  styleUrls: ['./date-report.component.scss'],
})
export class DateReportComponent implements OnInit {

  models: ReportOrderModel[] = [];

  fileType: string;

  constructor(private _authService: AuthService, private _reportService: ReportService, private _dateService: DateService) {
  }

  ngOnInit() {
  }

  print(date, fileType) {
    this.fileType = fileType;
    this._reportService.reportByDate(date, this._authService.getToken()).subscribe(this.handlePrint.bind(this));
  }

  handlePrint(response: ReportOrderResponse) {
    let date = 'Daily';
    for (let model of response.models) {
      model.stringDate = this.getDate(model.updatedAt);
      model.paymentMethod = this.convertPaymentMethodIdToText(parseInt(model.paymentMethodId));
    }

    let header = [
      {title: '#ORDER ID', dataKey: 'id'},
      // {title: 'CUSTOMER NAME', dataKey: 'customerName'},
      {title: 'PAYMENT', dataKey: 'paymentMethod'},
      {title: 'COST', dataKey: 'totalProductCost'},
      // {title: 'TAX', dataKey: 'tax'},
      {title: 'DATE', dataKey: 'stringDate'},
    ];
    let data = response.models;
    let doc = new jsPDF('p', 'pt');
    doc.page=1;

    doc.autoTable(header, data, {

      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {},
      bodyStyles: {},
      alternateRowStyles: {},
      columnStyles: {},

      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: [40], // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number,
      showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,

      // Hooks
      createdHeaderCell: function (cell, data) {
        doc.setFontSize(12);
        doc.text(date + ' Report', 40, 30);
      },
      createdCell: function (cell, data) {
      },
      drawHeaderRow: function (row, data) {
      },
      drawRow: function (row, data) {
      },
      drawHeaderCell: function (cell, data) {
      },
      drawCell: function (cell, data) {
      },
      addPageContent: function (data) {
        doc.setFontStyle('italic');
        doc.setFontSize(12);
        doc.text('page ' + doc.page +' | '+ date, data.settings.margin.left, 810);
        doc.page ++;
      }
    });

    doc.autoPrint();

    if (this.fileType === 'pdf') {
      doc.save();
    }
    else {
      window.open(doc.output('bloburl'), '_blank');
    }

  }

  getDate(date: string): string {
    return this._dateService.getDateNumberFormat(date);
  }

  convertPaymentMethodIdToText(paymentMethodId : number) : string {
    return parsePaymentMethodToText(paymentMethodId);
  }
}
