import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DateReportComponent} from './date-report/date-report.component';
import {WeekReportComponent} from './week-report/week-report.component';
import {MonthReportComponent} from './month-report/month-report.component';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {YearReportComponent} from './year-report/year-report.component';
import {AuthService} from '../../auth.service';
import {ReportService} from '../report.service';
import {DateService} from '../../date.service';
import {ToasterService} from '../../toaster.service';

@NgModule({
  imports: [
    CommonModule,
    ToolkitModule
  ],
  declarations: [DateReportComponent, WeekReportComponent, MonthReportComponent, YearReportComponent],
  exports: [DateReportComponent, WeekReportComponent, MonthReportComponent, YearReportComponent],
  providers: [AuthService, ReportService, DateService, ToasterService]
})
export class OrderReportModule {
}
