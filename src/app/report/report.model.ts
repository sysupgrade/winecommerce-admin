export class ReportOrderModel {
  id: string;
  status: string;
  customerId: string;
  createdAt: string;
  updatedAt: string;
  shipmentMethodId: string;
  paymentMethodId: string;
  paymentMethod: string;
  totalProductCost: number;
  shipmentCost: string;
  taxAmount: string;
  customerName: string;
  statusText: string;
  stringDate: string;
  totalCost: number;
}

export class ReportOrderResponse {
  models: ReportOrderModel[] = [];
  status: string;
}
