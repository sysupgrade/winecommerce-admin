import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SalesOrderReportComponent} from './sales-order-report/sales-order-report.component';
import {DirectivesModule} from '../theme/directives/directives.module';
import {PipesModule} from '../theme/pipes/pipes.module';
import {ToolkitModule} from '../toolkit/toolkit.module';
import {RouterModule} from '@angular/router';
import {OrderReportModule} from './order-report/order-report.module';

export const routes = [
  {path: 'order', component: SalesOrderReportComponent, data: {breadcrumb: 'Order'}},
];

@NgModule({
  imports: [
    CommonModule,
    ToolkitModule,
    DirectivesModule,
    PipesModule,
    OrderReportModule,
    RouterModule.forChild(routes),
  ],
  declarations: [SalesOrderReportComponent]
})
export class ReportModule {
}
