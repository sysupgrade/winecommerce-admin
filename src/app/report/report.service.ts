import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {ReportOrderResponse} from './report.model';
import {AppSettings} from '../app.setting';

@Injectable()
export class ReportService {

  constructor(private _http: Http) {
  }

  reportByDate(date: string, token: string): Observable<ReportOrderResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('date', date);

    return this._http.get(AppSettings.REPORT_SALES_ORDER_DATE + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  reportByWeek(week: string, token: string): Observable<ReportOrderResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('week', week);

    return this._http.get(AppSettings.REPORT_SALES_ORDER_WEEK + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  reportByMonth(month: string, token: string): Observable<ReportOrderResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('month', month);

    return this._http.get(AppSettings.REPORT_SALES_ORDER_MONTH + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  reportByYear(year: number, token: string): Observable<ReportOrderResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('year', year+'');

    return this._http.get(AppSettings.REPORT_SALES_ORDER_YEAR + '?' + params.toString(), options)
      .map((response) => response.json());
  }
}
