import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductResponse} from '../../product/product.model';
import {Validators} from '@angular/forms';
import {AppSettings} from '../../app.setting';
import {AttributeModel} from '../attribute.model';

@Component({
  selector: 'th-attribute-add-form',
  templateUrl: './attribute-add-form.component.html',
  styleUrls: ['./attribute-add-form.component.scss']
})
export class AttributeAddFormComponent implements OnInit {

  readonly NAME: string = 'name';

  url: string = AppSettings.ATTRIBUTE_ADD;

  @Input()
  model: AttributeModel = new AttributeModel();

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.model.name, [Validators.required, Validators.minLength(3)]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'name is required'};
    return json;
  }

  successForm(object: ProductResponse) {
    if (!object) return;
    this.success.emit(true);
    this.model = new AttributeModel();
  }
}
