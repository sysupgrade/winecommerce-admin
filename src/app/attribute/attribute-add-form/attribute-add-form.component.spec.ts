import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AttributeAddFormComponent} from './attribute-add-form.component';

describe('AttributeAddFormComponent', () => {
  let component: AttributeAddFormComponent;
  let fixture: ComponentFixture<AttributeAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AttributeAddFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributeAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
