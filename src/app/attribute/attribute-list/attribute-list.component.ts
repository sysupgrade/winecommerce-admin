import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ATTRIBUTE_URL, AttributeModel} from '../attribute.model';
import {AttributeService} from '../attribute.service';
import {DateService} from '../../date.service';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'th-attribute-list',
  templateUrl: './attribute-list.component.html',
  styleUrls: ['./attribute-list.component.scss'],
  providers: [AttributeService, DateService]
})
export class AttributeListComponent implements OnInit {

  @Input()
  page: number = 1;

  @Input()
  pageSize: number = 10;

  status: string;

  search: string;

  models: AttributeModel[] = [];

  total: number = 0;

  isAddAttribute: boolean = false;

  constructor(
    private _attributeService: AttributeService,
    private _router: Router,
    private _authService : AuthService,
    private _dateService : DateService
  ) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._attributeService.list(this.page, this.pageSize, this.status, this.search)
      .subscribe((response) => {
        this.models = response.models;
        this.total = response.total;
      });
  }

  getProductURL(id: string) {
    this._router.navigate([ATTRIBUTE_URL.ROOT, id])
  }

  setAddAttrubute(val: boolean) {
    this.isAddAttribute = val;
    if (val == false) {
      this.list(this.page);
    }
  }

  handleFeatureChange(model : AttributeModel, checked : boolean) {
    this._attributeService.feature(model.id, this._authService.getToken(), checked)
        .subscribe((response) => {
      if(response.status) {
        this.list(this.page);
      }
    });
  }

  getDate(date: string): string {
    return this._dateService.getDateNumberFormat(date);
  }

}

