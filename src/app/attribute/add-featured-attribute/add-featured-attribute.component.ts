import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AttributeService } from '../attribute.service';
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';
import { Validators } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { AttributeProductModel, AttributeResponse, AttributeModel } from '../attribute.model';
import { AppSettings } from '../../app.setting';

@Component({
  selector: 'th-add-featured-attribute',
  templateUrl: './add-featured-attribute.component.html',
  styleUrls: ['./add-featured-attribute.component.scss']
})
export class AddFeaturedAttributeComponent implements OnInit {

  readonly ATTRIBUTE_VALUE : string = "attributeValue";

  readonly ATTRIBUTE_ID : string = "attributeId";

  readonly ATTRIBUTE_VALUE_CHINESE : string = "attributeValueChinese";

  searchUrl: string = AppSettings.ATTRIBUTE_LIST + '?page=1&pageSize=4&q=:q';

  attributeModels : AttributeModel[] = [];

  model : AttributeProductModel = new AttributeProductModel;

  @Output()
  success : EventEmitter<AttributeProductModel> = new EventEmitter<AttributeProductModel>();

  constructor(private _attributeService : AttributeService,
      private _authService : AuthService) { }

  ngOnInit() {
  }
  
  handleAttributeDataChange(response: AttributeResponse) {
    this.attributeModels = response.models;
  }

  getRules(): Object {
    let json: Object = {};
    json[this.ATTRIBUTE_VALUE] = [null, [Validators.required, Validators.minLength(3)]];
    json[this.ATTRIBUTE_ID] = [null, [Validators.required, Validators.minLength(3)]];
    json[this.ATTRIBUTE_VALUE_CHINESE] = [null, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.ATTRIBUTE_VALUE] = {'required': 'Value is required'};
    json[this.ATTRIBUTE_ID] = {"required": "Name is required"}; 
    json[this.ATTRIBUTE_VALUE_CHINESE] = {"required": "Chinese word is required"};
    return json;
  }

  featureAttribute(model : any) {
    this._attributeService.addFeaturedAttribute(model[this.ATTRIBUTE_ID], model[this.ATTRIBUTE_VALUE], model[this.ATTRIBUTE_VALUE_CHINESE], this._authService.getToken())
        .subscribe((response) => {
        this.success.emit(response.model);
        this.model = new AttributeProductModel;
    });
  }
}

