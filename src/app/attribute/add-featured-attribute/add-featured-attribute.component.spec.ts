import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFeaturedAttributeComponent } from './add-featured-attribute.component';

describe('AddFeaturedAttributeComponent', () => {
  let component: AddFeaturedAttributeComponent;
  let fixture: ComponentFixture<AddFeaturedAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFeaturedAttributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFeaturedAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
