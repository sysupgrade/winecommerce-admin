import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AttributeAddFormComponent} from './attribute-add-form/attribute-add-form.component';
import {AttributeEditFormComponent} from './attribute-edit-form/attribute-edit-form.component';
import {AttributeViewComponent} from './attribute-view/attribute-view.component';
import {AttributeListComponent} from './attribute-list/attribute-list.component';
import {DirectivesModule} from '../theme/directives/directives.module';
import {RouterModule} from '@angular/router';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ToolkitModule} from '../toolkit/toolkit.module';
import { FormsModule } from '@angular/forms';
import { FeaturedAttributeListComponent } from './featured-attribute-list/featured-attribute-list.component';
import { AttributeService } from './attribute.service';
import { AddFeaturedAttributeComponent } from './add-featured-attribute/add-featured-attribute.component';
import { DateService } from '../date.service';

export const routes = [
  {path: '', component: AttributeListComponent},
  {path: 'add', component: AttributeAddFormComponent},
  {path: 'edit', component: AttributeEditFormComponent},
  {path: 'featured', component: FeaturedAttributeListComponent},
  {path: ':id', component: AttributeViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    FormsModule,
    ToolkitModule,
    DirectivesModule,
    RouterModule.forChild(routes),
  ],
  declarations: [AttributeAddFormComponent, AttributeEditFormComponent, AttributeViewComponent, AttributeListComponent, FeaturedAttributeListComponent, AddFeaturedAttributeComponent],
  exports: [AttributeAddFormComponent, AttributeEditFormComponent, AttributeViewComponent, AttributeListComponent],
  providers: [AttributeService, DateService]
})
export class AttributeModule {
}
