import {Injectable} from '@angular/core';
import {Headers, Response, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../app.setting';
import {AttributeResponse, AttributeModel, AttributeProductModel, FeaturedAttributeResponse} from './attribute.model';

@Injectable()
export class AttributeService {


  constructor(private _http: Http) {
  }

  feature(attributeId : number, token : string, featured : boolean) : Observable<AttributeResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    let params : URLSearchParams = new URLSearchParams();
    params.append("attributeId", attributeId + "");
    params.append("authKey", token);
    params.append("featured", featured ? "1" : "0");
    return this._http.post("./api/catalog/attribute/feature", params)
          .map((response) => response.json());
  }

  list(page: number, pageSize: number, status: string, search: string): Observable<AttributeResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');
    if (status) params.append('status', status);
    if (search) params.append('q', search);

    return this._http.get(AppSettings.ATTRIBUTE_LIST + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  get(productId: string): Observable<AttributeResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', productId);

    return this._http.get(AppSettings.ATTRIBUTE_GET + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  
  listFeaturedAttribute() : Observable<AttributeProductModel[]> {
    return this._http.get("./api/catalog/attribute/list-featured")
          .map((response : Response) => <AttributeProductModel[]> response.json());
  }

  addFeaturedAttribute(attributeId : string, attributeValue : string, attributeValueChinese : string, authKey : string) : Observable<FeaturedAttributeResponse> {
    let params = new URLSearchParams;
    params.append("attributeId", attributeId);
    params.append("attributeValue", attributeValue);
    params.append("attributeValueChinese", attributeValueChinese);
    params.append("authKey", authKey);
    return this._http.post("./api/catalog/attribute/feature", params)
              .map((response : Response) => <FeaturedAttributeResponse>  response.json())
  }

  deleteFeaturedAttribute(attributeId : string, token : string) : Observable<FeaturedAttributeResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token  
    });

    let params = new URLSearchParams;
    params.append("id", attributeId);
    return this._http.post("./api/catalog/attribute/delete-featured", params, {headers: headers})
                .map((response : Response) =>  response.json());
  }
}