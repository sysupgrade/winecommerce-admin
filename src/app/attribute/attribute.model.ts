export class AttributeModel {
  id: number;
  name: string;
  status: number;
  featured : boolean;
  createdAt: number;
  updatedAt: number;
  attributeValue: string;
}

export class AttributeResponse {
  model: AttributeModel;
  models: AttributeModel[] = [];
  total: number;
  status: string;
}

export class FeaturedAttributeResponse {
  model : AttributeProductModel;
  models : AttributeProductModel[] = [];
  status : number;
}

export class ATTRIBUTE_URL {
  public static ROOT = '/pages/attribute/';
}

export class AttributeProductModel {
  attributeId: string;
  attributeName : string;
  attributeValue: string;
  attributeValueChinese? : string;
  createdAt : number;
  updatedAt : number;
  status : string;
}
