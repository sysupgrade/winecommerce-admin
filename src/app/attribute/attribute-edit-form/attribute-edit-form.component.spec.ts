import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AttributeEditFormComponent} from './attribute-edit-form.component';

describe('AttributeEditFormComponent', () => {
  let component: AttributeEditFormComponent;
  let fixture: ComponentFixture<AttributeEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AttributeEditFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributeEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
