import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedAttributeListComponent } from './featured-attribute-list.component';

describe('FeaturedAttributeListComponent', () => {
  let component: FeaturedAttributeListComponent;
  let fixture: ComponentFixture<FeaturedAttributeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedAttributeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedAttributeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
