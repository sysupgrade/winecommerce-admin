import { Component, OnInit, Input } from '@angular/core';
import { AttributeService } from '../attribute.service';
import { AttributeModel, AttributeProductModel } from '../attribute.model';
import { DateService } from '../../date.service';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'th-featured-attribute-list',
  templateUrl: './featured-attribute-list.component.html',
  styleUrls: ['./featured-attribute-list.component.scss']
})
export class FeaturedAttributeListComponent implements OnInit {

  models : AttributeProductModel[] = []

  constructor(private _attributeService : AttributeService,
      private _authService : AuthService,
      public dateService : DateService) { }

  ngOnInit() {
    this.fetchPage();
  }

  fetchPage() {
    this._attributeService.listFeaturedAttribute()
    .subscribe((models) => {
      console.log(models);
      this.models = models;
    });

  }

  deleteFeaturedAttribute(attributeId : string) {
    this._attributeService.deleteFeaturedAttribute(attributeId, this._authService.getToken())
        .subscribe((response) => {
      if(!response.status) return;
      this.fetchPage();
    });
  }
}
