export class SalesruleModel {
  id: string;
  name: string;
  couponCode: string;
  expiryDate: any;
  createdAt: number;
  updatedAt: number;
  status: number;
  statusText: string;
  discount: number;
}

export class SalesruleResponse {
  status: number;
  model: SalesruleModel;
  models: SalesruleModel[] = [];
  total: number;

}
