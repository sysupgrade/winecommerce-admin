import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {SalesruleResponse} from './salesrule.model';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {AppSettings} from '../app.setting';

@Injectable()
export class SalesruleService {

  constructor(private _http: Http) {
  }

  getListRule(): string {
    return './pages/salesrule';
  }

  list(page: number, pageSize: number, token: string): Observable<SalesruleResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');

    return this._http.get(AppSettings.SALES_RULE_LIST + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  delete(couponId: string, token: string): Observable<SalesruleResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('couponId', couponId + '');

    return this._http.post(AppSettings.SALES_RULE_DELETE, params, options)
      .map((response) => response.json());
  }

  changeStatus(status: number, token: string, id: string): Observable<SalesruleResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', id + '');
    params.append('status', status + '');

    return this._http.post(AppSettings.SALES_RULE_CHANGE_STATUS, params, options)
      .map((response) => response.json());
  }
}
