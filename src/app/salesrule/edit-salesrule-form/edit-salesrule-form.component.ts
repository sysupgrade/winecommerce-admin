import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SalesruleModel, SalesruleResponse} from '../salesrule.model';
import {Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AppSettings} from '../../app.setting';

@Component({
  selector: 'th-edit-salesrule-form',
  templateUrl: './edit-salesrule-form.component.html',
  styleUrls: ['./edit-salesrule-form.component.scss']
})
export class EditSalesruleFormComponent implements OnInit {

  settingsDatePicker = {
    bigBanner: true,
    timePicker: true,
    format: 'dd/MMM/yyyy HH:m',
    defaultOpen: false
  };

  readonly NAME: string = 'name';

  readonly COUPON_CODE: string = 'couponCode';

  readonly EXPIRY_DATE: string = 'expiryDate';

  readonly DECREMENT_VALUE: string = 'discount';

  readonly TIMEZONE_OFFSET: string = 'timezoneOffset';

  readonly ID: string = 'id';

  @Input() model: SalesruleModel = new SalesruleModel;

  timezoneOffset: number = (new Date()).getTimezoneOffset();

  @Output() success: EventEmitter<SalesruleModel> = new EventEmitter<SalesruleModel>();

  url: string = AppSettings.SALES_RULE_EDIT;

  edit : boolean = false;

  constructor(private _router: Router) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.ID] = [this.model.id, [Validators.required]];
    json[this.NAME] = [this.model.name, [Validators.required]];
    json[this.COUPON_CODE] = [this.model.couponCode, [Validators.required, Validators.minLength(3)]];
    json[this.EXPIRY_DATE] = [this.model.expiryDate, [Validators.required]];
    json[this.DECREMENT_VALUE] = [this.model.discount, [Validators.required, Validators.max(100), Validators.maxLength(3)]];
    json[this.TIMEZONE_OFFSET] = [this.timezoneOffset, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'Name is required'};
    json[this.COUPON_CODE] = {'required': 'Coupon code is required'};
    json[this.EXPIRY_DATE] = {'required': 'Expiry date is required'};
    json[this.DECREMENT_VALUE] = {'required': 'Discount amount is required'};
    return json;
  }

  successForm(object: SalesruleResponse) {
    if (!object.status) {
      return;
    }
    this.success.emit(object.model);
  }

  getExpiryDate(event){
    this.timezoneOffset = Date.parse(event) / 1000;
    this.edit = true;
  }

}
