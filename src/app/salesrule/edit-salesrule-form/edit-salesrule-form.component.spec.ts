import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditSalesruleFormComponent} from './edit-salesrule-form.component';

describe('EditSalesruleFormComponent', () => {
  let component: EditSalesruleFormComponent;
  let fixture: ComponentFixture<EditSalesruleFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditSalesruleFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSalesruleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
