import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddSalesruleFormComponent} from './add-salesrule-form.component';

describe('AddSalesruleFormComponent', () => {
  let component: AddSalesruleFormComponent;
  let fixture: ComponentFixture<AddSalesruleFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddSalesruleFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSalesruleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
