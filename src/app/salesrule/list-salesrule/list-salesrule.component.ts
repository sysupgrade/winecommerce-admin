import {Component, Input, OnInit} from '@angular/core';
import {SalesruleService} from '../salesrule.service';
import {AuthService} from '../../auth.service';
import {SalesruleModel, SalesruleResponse} from '../salesrule.model';
import {DateService} from '../../date.service';

@Component({
  selector: 'th-list-salesrule',
  templateUrl: './list-salesrule.component.html',
  styleUrls: ['./list-salesrule.component.scss'],
  providers: [SalesruleService, DateService]
})
export class ListSalesruleComponent implements OnInit {

  @Input() page: number = 1;

  @Input() pageSize: number = 10;

  @Input() total: number = 0;

  models: SalesruleModel[] = [];

  isDeleteCouponModalOpened: boolean = false;

  isEditCouponModalOpened: boolean = false;

  onDeleteCoupon: SalesruleModel = new SalesruleModel;

  onEditCoupon: SalesruleModel = new SalesruleModel;

  constructor(private _salesruleService: SalesruleService,
              private _dateService: DateService,
              private _authService: AuthService) {
  }

  ngOnInit() {
    this.getPage(this.page);
  }

  getPage(page: number) {
    this.page = page;
    this._salesruleService.list(this.page, this.pageSize, this._authService.getToken())
      .subscribe((response: SalesruleResponse) => {
        this.models = response.models;
        this.total = response.total;
      });
  }

  showDeleteModal(model: SalesruleModel) {
    this.isDeleteCouponModalOpened = true;
    this.onDeleteCoupon = model;
  }

  showEditModal(model: SalesruleModel) {
    this.isEditCouponModalOpened = true;
    model.expiryDate = model.expiryDate * 1000;
    this.onEditCoupon = model;

  }

  getDateTime(timestamp: number) {
    return this._dateService.getDateTime(timestamp + '');
  }

  deleteCoupon() {
    this._salesruleService.delete(this.onDeleteCoupon.id, this._authService.getToken())
      .subscribe((response: SalesruleResponse) => {
        if (!response.status) return;
        this.models = this.models.filter((model: SalesruleModel) => {
          return model.id != this.onDeleteCoupon.id;
        });
        this.total -= 1;
        this.onDeleteCoupon = new SalesruleModel;
      });
  }

  changeStatusCoupon(status: number, targetModel: SalesruleModel, text: string) {
    this._salesruleService.changeStatus(status, this._authService.getToken(),
      targetModel.id)
      .subscribe((response: SalesruleResponse) => {
        if (!response.status) return;
        this.models = this.models.map((model: SalesruleModel) => {
          if (model.id == targetModel.id) {
            model.status = status;
            model.statusText = text;
          }
          return model;
        });
      });
  }

  handleEditSuccess(editedModel: SalesruleModel) {
    this.models = this.models.map((model: SalesruleModel) => {
      if (model.id == editedModel.id) {
        return editedModel;
      }

      return model;
    });

    this.isEditCouponModalOpened = false;
  }
}
