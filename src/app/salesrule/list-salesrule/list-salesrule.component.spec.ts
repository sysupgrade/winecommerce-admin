import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListSalesruleComponent} from './list-salesrule.component';

describe('ListSalesruleComponent', () => {
  let component: ListSalesruleComponent;
  let fixture: ComponentFixture<ListSalesruleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListSalesruleComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSalesruleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
