import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ToolkitModule} from '../toolkit/toolkit.module';
import {PipesModule} from '../theme/pipes/pipes.module';
import {DirectivesModule} from '../theme/directives/directives.module';
import {RouterModule} from '@angular/router';
import {ListSalesruleComponent} from './list-salesrule/list-salesrule.component';
import {AddSalesruleFormComponent} from './add-salesrule-form/add-salesrule-form.component';
import {EditSalesruleFormComponent} from './edit-salesrule-form/edit-salesrule-form.component';


export const routes = [
  {path: '', component: ListSalesruleComponent},
  {path: 'add', component: AddSalesruleFormComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    PipesModule,
    DirectivesModule,
    RouterModule.forChild(routes),
  ],

  declarations: [ListSalesruleComponent, AddSalesruleFormComponent, EditSalesruleFormComponent],
  exports: [ListSalesruleComponent, AddSalesruleFormComponent, EditSalesruleFormComponent],
})
export class SalesruleModule {
}
