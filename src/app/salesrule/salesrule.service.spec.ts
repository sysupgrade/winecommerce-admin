import {inject, TestBed} from '@angular/core/testing';

import {SalesruleService} from './salesrule.service';

describe('SalesruleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalesruleService]
    });
  });

  it('should be created', inject([SalesruleService], (service: SalesruleService) => {
    expect(service).toBeTruthy();
  }));
});
