import {AppLocal} from './app.local';

export class AppSettings {

  public static LOGIN_API = './erp/login-api-json';

  public static SIGNUP_API = './erp/sign-up';

  public static GET_FILES_BY_USER = '';

  public static GET_FILES_BY_USER_API = '';

  public static GET_FILE_BY_ID_API = '';

  public static TOKEN_STORAGE = 'authKey';

  public static FIRST_NAME_STORAGE = 'firstName';

  public static LAST_NAME_STORAGE = 'lastName';

  public static CART_ID_STORAGE = 'cartId';

  public static GET_FILE_BY_PATH_API = '';

  public static NO_IMAGE_API = '/assets/img/app/no-image.png';

  public static LEFT_KEY = 37;

  public static RIGHT_KEY = 39;

  public static ENTER_KEY = 13;

  public static DOWN_KEY = 40;


  public static GET_IMAGE_BY_ID = './api/file/default/id';
  public static GET_IMAGE_BY_TOKEN = './api/file/default/id?id=';
  public static DOWNLOAD_FILE = './api/file/default/download?id=';
  public static DELETE_FILE = './api/file/default/delete';
  public static GET_USER_IMAGE = './api/file/default/get-by-user';
  public static GET_IMAGE_PATH = './api/file/default/index?path=';
  public static UPLOAD_IMAGE = './api/file/default/upload';


  public static USER_LOGIN = './api/user/default/login';
  public static USER_GET_BY_ID = './api/user/user/get-by-id';
  public static USER_LIST = './api/user/user/list';

  public static PRODUCT_LIST = './api/catalog/product/list';
  public static PRODUCT_GET = './api/catalog/product/get';
  public static PRODUCT_ADD = './api/catalog/product/add';
  public static PRODUCT_EDIT = './api/catalog/product/edit';
  public static PRODUCT_ADD_CATEGORY = './api/catalog/product/add-product-category';
  public static PRODUCT_ADD_ATTRIBUTE = './api/catalog/product/add-product-attribute';
  public static PRODUCT_REMOVE_ATTRIBUTE = './api/catalog/product/remove-product-attribute';
  public static PRODUCT_REMOVE_CATEGORY = './api/catalog/product/remove-product-category';
  public static PRODUCT_ADD_IMAGE = './api/catalog/product/add-product-image';
  public static PRODUCT_IMAGE_STATUS = './api/catalog/product/product-image-status';
  public static PRODUCT_CHANGE_STATUS = './api/catalog/product/status';

  public static CATEGORY_LIST = './api/catalog/category/list';
  public static CATEGORY_GET = './api/catalog/category/get';
  public static CATEGORY_ADD = './api/catalog/category/add';
  public static CATEGORY_EDIT = './api/catalog/category/edit';

  public static ATTRIBUTE_LIST = './api/catalog/attribute/list';
  public static ATTRIBUTE_GET = './api/catalog/attribute/get';
  public static ATTRIBUTE_ADD = './api/catalog/attribute/add';
  public static ATTRIBUTE_EDIT = './api/catalog/attribute/edit';

  public static FEEDBACK_LIST = './api/catalog/feedback/list';
  public static FEEDBACK_GET = './api/catalog/feedback/get';
  public static FEEDBACK_ADD = './api/catalog/feedback/add';
  public static FEEDBACK_EDIT = './api/catalog/feedback/edit';
  public static FEEDBACK_CHANGE_STATUS = './api/catalog/feedback/status';

  public static SALES_ORDER_LIST = './api/sales/order/list';
  public static SALES_ORDER_LIST_BY_USER = './api/sales/order/mine';
  public static SALES_CHANGE_ORDER_STATUS = './api/sales/order/status';
  public static SALES_ORDER_GET = './api/sales/order/get';
  public static SALES_ORDER_REMOVE = './api/sales/order/remove';

  public static SHIPMENT_METHOD_ADD = './api/sales/shipment/add';
  public static SHIPMENT_METHOD_LIST = './api/sales/shipment/list';
  public static SHIPMENT_METHOD_GET = './api/sales/shipment/get';
  public static SHIPMENT_METHOD_EDIT = './api/sales/shipment/edit';
  public static SHIPMENT_METHOD_STATUS = './api/sales/shipment/status';

  public static SALES_RULE_LIST = './api/sales/rule/list';
  public static SALES_RULE_ADD = './api/sales/rule/add';
  public static SALES_RULE_EDIT = './api/sales/rule/edit';
  public static SALES_RULE_CHANGE_STATUS = './api/sales/rule/change-status';
  public static SALES_RULE_DELETE = './api/sales/rule/delete';

  public static BLOG_LIST = AppLocal.API_YII_ENDPOINT + '/blog/blog/list';
  public static BLOG_ADD = AppLocal.API_YII_ENDPOINT + '/blog/blog/add';
  public static BLOG_GET_BY_ID = AppLocal.API_YII_ENDPOINT + '/blog/blog/get';
  public static BLOG_CHANGE_STATUS = AppLocal.API_YII_ENDPOINT + '/blog/blog/edit';
  public static BLOG_EDIT = AppLocal.API_YII_ENDPOINT + '/blog/blog/edit';

  public static NEWS_LETTER_ADD = AppLocal.API_YII_ENDPOINT + '/blog/newsletter/add';
  public static NEWS_LETTER_LIST = AppLocal.API_YII_ENDPOINT + '/blog/newsletter/list';

  public static TESTIMONIAL_LIST = './api/blog/testimonial/list';
  public static TESTIMONIAL_GET_BY_ID = './api/blog/testimonial/get';
  public static TESTIMONIAL_CHANGE_STATUS = './api/blog/testimonial/edit';
  public static TESTIMONIAL_ADD = './api/blog/testimonial/add';
  public static TESTIMONIAL_EDIT = "./api/blog/testimonial/edit";

  public static REPORT_SALES_ORDER_DATE = AppLocal.API_YII_ENDPOINT + '/sales/report/sales-order-date';
  public static REPORT_SALES_ORDER_WEEK = AppLocal.API_YII_ENDPOINT + '/sales/report/sales-order-week';
  public static REPORT_SALES_ORDER_MONTH = AppLocal.API_YII_ENDPOINT + '/sales/report/sales-order-month';
  public static REPORT_SALES_ORDER_YEAR = AppLocal.API_YII_ENDPOINT + '/sales/report/sales-order-year';

  public static SALES_USER_POINT = AppLocal.API_YII_ENDPOINT + '/sales/point/get-user-point';
  public static SALES_USER_POINT_HISTORY = AppLocal.API_YII_ENDPOINT + '/sales/point/list-user-point-history';





























  public static SALES_CART_ADD = 'http://localhost/annex/sales/cart/add';
  public static SALES_CHACKOUT_ADD = 'http://localhost/annex/sales/checkout/add';
  public static SALES_CHACKOUT_SHPMENT = 'http://localhost/annex/sales/checkout/shipment';








}
