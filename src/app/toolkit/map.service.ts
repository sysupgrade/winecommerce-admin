import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { MapsAPILoader, AgmCoreModule, AgmMap } from '@agm/core';
import { } from 'googlemaps';

@Injectable()
export class MapService {

  constructor() { }

  getLocationText(lat : number, lng : number) : Observable<string>{
    let geocode = new google.maps.Geocoder();
    let latLng = new google.maps.LatLng(lat, lng);

    return new Observable((observer: Observer<string>) => {
      geocode.geocode({ 'location': latLng }, (
        (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
          if (status === google.maps.GeocoderStatus.OK) {
            if(results.length != 0) {
              observer.next(results[0].formatted_address);
            }    
          } else {
            console.log('Geocoding service: geocoder failed due to: ' + status);
            return null;
          }
        })
      );
    });
  }
}
