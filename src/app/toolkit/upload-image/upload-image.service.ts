import { AppSettings } from './../../app.setting';
import { RequestOptions, Http } from '@angular/http';
import { UploadImageResponse } from './upload-image.model';
import { Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class UploadImageService {

  constructor(private http : Http) { }

  uploadImage(file : File, token : string) : Observable<UploadImageResponse> {
    let formData:FormData = new FormData();
    formData.append('file',file, file.name);
    let headers = new Headers({
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers : headers});
    return this.http.post(AppSettings.UPLOAD_IMAGE,  formData, options)
              .map((response) => response.json())
  }
}
