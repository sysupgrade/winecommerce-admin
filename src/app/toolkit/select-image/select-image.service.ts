import { RequestOptions, URLSearchParams } from '@angular/http';
import { GetFilesByUserResponse, ImageApi } from './select-image.model';
import { Observable } from 'rxjs';
import { AppSettings } from './../../app.setting';
import { Headers } from '@angular/http';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class SelectImageService {

  constructor(private http : Http) { }

  getUserImage(token : string) : Observable<GetFilesByUserResponse> {
    let headers = new Headers({ 'Content-Type' : 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({headers : headers});

    let params = new URLSearchParams();
    params.append('authKey', token);
    return this.http.post(AppSettings.GET_USER_IMAGE, params).map((response) => response.json());
  }

  deleteImage(fileId : string, token : string) {    
    let headers = new Headers({
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + token
    });
    let options = new RequestOptions({headers : headers});

    let params = new URLSearchParams();
    params.append("fileId", fileId);
    return this.http.post(AppSettings.DELETE_FILE, params, options).map((response) => response.json());
  }
}
