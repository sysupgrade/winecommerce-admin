import { CommonModule } from '@angular/common';
import { Component, OnInit, Input, NgZone, ViewChild, ElementRef } from '@angular/core';
import { Field } from "./../field";
import { MapsAPILoader, AgmCoreModule, AgmMap } from '@agm/core';
import { } from 'googlemaps';

@Component({
  selector: 'app-map-field',
  templateUrl: './map-field.component.html',
  styleUrls: ['./map-field.component.css']
})
export class MapFieldComponent extends Field<number[]> implements OnInit {

  @Input()
  lat : number = 60;

  @Input()
  lng : number = 60;

  @Input()
  zoom : number = 8;

  markerDraggable : boolean = true;
  
  @ViewChild("search")
  public searchElementRef: ElementRef;

  @ViewChild("map")
  mapComponent : AgmMap;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {
    super();
  }

  ngOnInit() {
    
    //load Places Autocomplete
    this.mapsAPILoader.load().then(function() {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 12;
          this.markerDraggable = true;
        });
      });
    }.bind(this));
  }

  getValue() : number[] {
    return [this.lat, this.lng];
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;
      });
      

    }
  }

  initMapIfNeeded() {
    this.mapComponent.triggerResize();
  }

  handleDragEnd(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;

    let geocode = new google.maps.Geocoder();
    let latLng = new google.maps.LatLng(this.lat, this.lng);

    geocode.geocode({ 'location': latLng }, (
      (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
          if (status === google.maps.GeocoderStatus.OK) {
            if(results.length != 0) {
              this.searchElementRef.nativeElement.value = results[0].formatted_address;
            }    
          } else {
              console.log('Geocoding service: geocoder failed due to: ' + status);
          }
      })
    );
  }
}
