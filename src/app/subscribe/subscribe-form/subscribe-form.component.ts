import {Component, OnInit} from '@angular/core';
import {BlogResponse} from '../../blogs/blog/blog.model';
import {Validators} from '@angular/forms';
import {ToasterService} from '../../toaster.service';
import {SubscribeModel} from '../subscribe.model';
import {DateService} from '../../date.service';

@Component({
  selector: 'th-subscribe-form',
  templateUrl: './subscribe-form.component.html',
  styleUrls: ['./subscribe-form.component.scss'],
  providers: [ToasterService]
})
export class SubscribeFormComponent implements OnInit {

  readonly SUBJECT: string = 'subject';

  readonly BODY: string = 'body';

  model = new SubscribeModel();

  url: string = './api/user/subscribe/send';

  constructor(private _toasterService: ToasterService) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.SUBJECT] = [this.model.subject, [Validators.required, Validators.minLength(3)]];
    json[this.BODY] = [this.model.body, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.SUBJECT] = {'required': 'required'};
    json[this.BODY] = {'required': 'required'};
    return json;
  }

  successForm(object: BlogResponse) {
    if (!object.status) {
      return;
    }
    this._toasterService.openToast('', 'send success', 'success')
  }
}

