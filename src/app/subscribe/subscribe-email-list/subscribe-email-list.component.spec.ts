import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribeEmailListComponent } from './subscribe-email-list.component';

describe('SubscribeEmailListComponent', () => {
  let component: SubscribeEmailListComponent;
  let fixture: ComponentFixture<SubscribeEmailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscribeEmailListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribeEmailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
