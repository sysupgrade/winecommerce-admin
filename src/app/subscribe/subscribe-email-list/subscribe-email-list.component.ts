import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../auth.service';
import {EmailSubscribeModel} from '../subscribe.model';
import {SubscribeService} from '../subscribe.service';
import {ToasterService} from '../../toaster.service';
import {DateService} from '../../date.service';

@Component({
  selector: 'th-subscribe-email-list',
  templateUrl: './subscribe-email-list.component.html',
  styleUrls: ['./subscribe-email-list.component.scss'],
  providers:[SubscribeService, DateService]
})
export class SubscribeEmailListComponent implements OnInit {

  @Input()
  page: number = 1;

  @Input()
  pageSize: number = 10;

  models: EmailSubscribeModel[] = [];

  total : number = 0;

  constructor(
    private _subscribeService: SubscribeService,
    private _authService: AuthService,
    public _dateService : DateService
  ) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._subscribeService.list(this.page, this.pageSize, this._authService.getToken())
      .subscribe((response) => {
        this.models = response.models;
        this.total = response.total;
      });

  }
}
