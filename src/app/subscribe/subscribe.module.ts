import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SubscribeFormComponent} from './subscribe-form/subscribe-form.component';
import {SubscribeEmailListComponent} from './subscribe-email-list/subscribe-email-list.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ToolkitModule} from '../toolkit/toolkit.module';
import {DirectivesModule} from '../theme/directives/directives.module';
import {RouterModule} from '@angular/router';

export const routes = [
  {path: '', component: SubscribeFormComponent},
  {path: 'list', component: SubscribeEmailListComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    DirectivesModule,
    RouterModule.forChild(routes),
  ],
  declarations: [SubscribeFormComponent, SubscribeEmailListComponent],
  exports: [SubscribeFormComponent, SubscribeEmailListComponent],
})
export class SubscribeModule {
}
