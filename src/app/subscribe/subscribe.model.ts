export class SubscribeModel {
  subject: string;
  body: string;
}

export class EmailSubscribeModel {
  userId : string;
  email : string;
}

export class EmailSubsribeResponse{
  models: EmailSubscribeModel[]=[];
  status: string;
  total : number;
}
