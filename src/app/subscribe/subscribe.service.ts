import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {EmailSubsribeResponse} from './subscribe.model';

@Injectable()
export class SubscribeService {

  constructor(private _http: Http) {
  }

  list(page:number, pageSize:number, token : string): Observable<EmailSubsribeResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '+token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page+'');
    params.append('pageSize', pageSize+'');

    return this._http.get("./api/user/subscribe/list" + '?' + params.toString(), options)
      .map((response) => response.json());
  }
}
