import {Component, Input, OnInit} from '@angular/core';
import {DateService} from '../../date.service';
import {AuthService} from '../../auth.service';
import {ToasterService} from '../../toaster.service';
import {PRODUCT_URL, ProductModel} from '../../product/product.model';
import {Router} from '@angular/router';
import {FeedbackModel, FilterFeedbackModel} from '../feedback.model';
import {FeedbackService} from '../feedback.service';

@Component({
  selector: 'th-feedback-list',
  templateUrl: './feedback-list.component.html',
  styleUrls: ['./feedback-list.component.scss'],
  providers: [FeedbackService, ToasterService, DateService]
})
export class FeedbackListComponent implements OnInit {

  @Input()
  page: number = 1;

  @Input()
  pageSize: number = 10;

  status: string = '10';

  userId: string = null;

  startDate: string = null;

  endDate: string = null;

  models: FeedbackModel[] = [];

  total: number = 0;

  isDialogConfirm: boolean = false;

  model: FeedbackModel = new FeedbackModel();

  statusSelected: string;

  constructor(
    private _feedbackService: FeedbackService,
    private _router: Router,
    private _authService: AuthService,
    private _toasterService: ToasterService,
    private _dateService: DateService
  ) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._feedbackService.list(this.page, this.pageSize, this.status, this.userId, this.startDate, this.endDate)
      .subscribe((response) => {
        this.models = response.models;
        this.total = response.total;
      });

  }

  getProductURL(id: string) {
    this._router.navigate([PRODUCT_URL.ROOT, id])
  }

  handleOnFilter(model: FilterFeedbackModel) {
    this.userId = model.userId;
    this.status = model.status;
    this.startDate = model.startDate;
    this.endDate = model.endDate;
    this.list(this.page);
  }

  openDialogConfirm(val: boolean, data: ProductModel) {
    this.model = data;
    this.isDialogConfirm = val;
  }

  changeStatus(productId: string, status: string) {
    if (status == '10') this.statusSelected = 'APPROVED';
    if (status == '11') this.statusSelected = 'PENDING';
    if (status == '0') this.statusSelected = 'DELETE';

    this._feedbackService.changeStatus(productId, status, this._authService.getToken())
      .subscribe((response) => {
        if (response.status == '1')
          this.list(this.page);
        this._toasterService.openToast(this.statusSelected, '', 'success');
      });
  }

  getDate(date: string): string {
    return this._dateService.getDateNumberFormat(date);
  }

}
