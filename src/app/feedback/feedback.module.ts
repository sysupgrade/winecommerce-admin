import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToolkitModule} from '../toolkit/toolkit.module';
import {RouterModule} from '@angular/router';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {DirectivesModule} from '../theme/directives/directives.module';
import {FeedbackAddFormComponent} from './feedback-add-form/feedback-add-form.component';
import {FeedbackEditFormComponent} from './feedback-edit-form/feedback-edit-form.component';
import {FeedbackViewComponent} from './feedback-view/feedback-view.component';
import {FeedbackListComponent} from './feedback-list/feedback-list.component';
import {FeedbackFilterFormComponent} from './feedback-filter-form/feedback-filter-form.component';

export const routes = [
  {path: '', component: FeedbackListComponent},
  {path: 'add', component: FeedbackAddFormComponent},
  {path: 'edit', component: FeedbackEditFormComponent},
  {path: ':id', component: FeedbackViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    DirectivesModule,
    RouterModule.forChild(routes),
  ],
  declarations: [FeedbackListComponent, FeedbackViewComponent, FeedbackEditFormComponent, FeedbackAddFormComponent, FeedbackFilterFormComponent],
  exports: [FeedbackListComponent, FeedbackViewComponent, FeedbackEditFormComponent, FeedbackAddFormComponent, FeedbackFilterFormComponent],


})
export class FeedbackModule {
}
