export class FeedbackModel {
  id: string;
  name: string;
  description: string;
  createdAt: string;
  updatedAt: string;
}

export class FeedbackResponse {
  model: FeedbackModel;
  models: FeedbackModel[] = [];
  total: number;
  status: string;
}


export class FEEDBACK_URL {
  public static ROOT = '/pages/feedback/';
}

export class FilterFeedbackModel {
  userId: string;
  status: string;
  startDate: string;
  endDate: string;
}
