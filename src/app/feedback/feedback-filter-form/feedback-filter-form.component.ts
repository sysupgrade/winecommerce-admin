import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FilterFeedbackModel} from '../feedback.model';
import {DropdownItemModel} from '../../toolkit/toolkit.model';
import {AppSettings} from '../../app.setting';
import {UserModel, UserResponse} from '../../user/user.model';

@Component({
  selector: 'th-feedback-filter-form',
  templateUrl: './feedback-filter-form.component.html',
  styleUrls: ['./feedback-filter-form.component.scss']
})
export class FeedbackFilterFormComponent implements OnInit {

  statusValue: string = null;

  userId: string = null;

  userUrl: string = AppSettings.USER_LIST;

  userModels: UserModel[] = [];

  userTotal: number = 0;

  statusText: string = null;

  @Input()
  id: string;

  total: number;

  statusItems: DropdownItemModel[] = [
    {
      text: 'Accept',
      value: '10'
    }, {
      text: 'Pending',
      value: '11'
    }, {
      text: 'Delete',
      value: '0'
    }];

  @Output()
  filter: EventEmitter<FilterFeedbackModel> = new EventEmitter<FilterFeedbackModel>();

  constructor() {
  }

  ngOnInit() {
  }

  handleOnFilter(event, startDate: string, endDate: string) {
    let dateStart = Date.parse(startDate).toString();
    let intDateStart = dateStart.substring(0, dateStart.length - 3);
    let dateEnd = Date.parse(endDate).toString();
    let intDateEnd = dateEnd.substring(0, dateEnd.length - 3);

    this.filter.emit({
      userId: this.userId,
      status: this.statusValue,
      startDate: intDateStart,
      endDate: intDateEnd
    });

  }

  resetFilter() {
    this.userId = null;
    this.statusValue = null;
    this.statusText = null;
    this.handleOnFilter(null, null, null);
  }

  handleUserChange(response: UserResponse) {
    this.userTotal = response.total;
    this.userModels = response.models;
  }

}
