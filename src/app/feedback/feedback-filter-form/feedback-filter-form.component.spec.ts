import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeedbackFilterFormComponent} from './feedback-filter-form.component';

describe('FeedbackFilterFormComponent', () => {
  let component: FeedbackFilterFormComponent;
  let fixture: ComponentFixture<FeedbackFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FeedbackFilterFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
