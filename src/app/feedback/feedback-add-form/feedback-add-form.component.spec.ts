import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeedbackAddFormComponent} from './feedback-add-form.component';

describe('FeedbackAddFormComponent', () => {
  let component: FeedbackAddFormComponent;
  let fixture: ComponentFixture<FeedbackAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FeedbackAddFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
