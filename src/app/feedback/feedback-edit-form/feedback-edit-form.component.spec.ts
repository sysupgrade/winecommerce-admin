import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeedbackEditFormComponent} from './feedback-edit-form.component';

describe('FeedbackEditFormComponent', () => {
  let component: FeedbackEditFormComponent;
  let fixture: ComponentFixture<FeedbackEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FeedbackEditFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
