import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../app.setting';
import {FeedbackResponse} from './feedback.model';

@Injectable()
export class FeedbackService {

  constructor(private _http: Http) {
  }

  list(page: number, pageSize: number, status: string, userId: string, startDate: string, endDate: string): Observable<FeedbackResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');
    if (status) params.append('status', status);
    if (userId) params.append('userId', userId);
    if (startDate) params.append('startDate', startDate);
    if (endDate) params.append('endDate', endDate);

    return this._http.get(AppSettings.FEEDBACK_LIST + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  get(productId: string): Observable<FeedbackResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', productId);

    return this._http.get(AppSettings.FEEDBACK_GET + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  changeStatus(feedbackId: string, status: string, token: string): Observable<FeedbackResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', feedbackId);
    params.append('status', status);

    return this._http.post(AppSettings.FEEDBACK_CHANGE_STATUS, params, options)
      .map((response) => response.json());
  }
}
