export class ApiUser {

  public static LOGIN_API = "./api/user/default/login";

  public static TOKEN_STORAGE = "authToken";

  public static FIRST_NAME_STORAGE = "firstName";

  public static LAST_NAME_STORAGE = "lastName";

  public static USERNAME_STORAGE = "username";

  public static GET_USER_BY_ID = "./api/user/user/get-by-id";

  public static LIST_USER = "./api/user/user/list";
}

export class UserCsvModel {
  id : string;
  firstName : string;
  lastName : string;
  email : string;
  createdAt : string;
}

export class UserModel {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  fullName: string;
  username: string;
  isValidated : number;
  isNewsletter : number;
  createdAt : number;
  updatedAt : number;
  address : string;
  telephone : string;
  imageId : string;

  salutation: string;
  companyName: string;
  birthday: string;
  shippingStreetName: string;
  shippingUnitNumber: string;
  shippingCity: string;
  shippingCountry: string;
  shippingPostalCode: string;
  billingStreetName: string;
  billingUnitNumber: string;
  billingCity: string;
  billingCountry: string;
  billingPostalCode: string;

  constructor() {
    this.id = null;
    this.firstName = null;
    this.lastName = null;
    this.email = null;
    this.fullName = null;
    this.username = null;
  }
}

export class LoginModel {
  email: string;

  password: string;

  constructor() {
    this.email = null;
    this.password = null;
  }
}

export class LoginResponse {

  status: boolean;

  authKey: string;

  firstName: string;

  lastName: string;

}

export class UserInfoResponse{
  id: string;
  firstName: string;
  lastName: string;
  username: string;
  status: number;
  title: string;
  department: string;
  officeTelephone: string;
  mobileTelephone: string;
  homeTelephone: string;
  other: string;
  fax: string;
  notes: string;
  address: string;
  city: string;
  state: string;
  postalCode: string;
  country: string;
  authKey: string;

  salutation: string;
  companyName: string;
  birthday: string;
  shippingStreetName: string;
  shippingUnitNumber: string;
  shippingCity: string;
  shippingCountry: string;
  shippingPostalCode: string;
  billingStreetName: string;
  billingUnitNumber: string;
  billingCity: string;
  billingCountry: string;
  billingPostalCode: string;
}

export class UserResponse {
  total : number;
  model : UserModel;
  models : UserModel[] = [];
  status : number;
}

