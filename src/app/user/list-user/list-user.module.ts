import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListUserComponent} from './list-user.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {FilterUserListComponent} from './filter-user-list/filter-user-list.component';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {ViewUserComponent} from './view-user/view-user.component';
import {ListUserPointComponent} from './list-user-point/list-user-point.component';
import {ListOrderByUserComponent} from './list-order-by-user/list-order-by-user.component';

@NgModule({
  imports: [
    CommonModule, NgxDatatableModule, ToolkitModule
  ],
  declarations: [ListUserComponent, FilterUserListComponent, ViewUserComponent, ListUserPointComponent, ListOrderByUserComponent],
  exports: [ListUserComponent, FilterUserListComponent, ViewUserComponent, ListOrderByUserComponent]
})
export class ListUserModule {
}
