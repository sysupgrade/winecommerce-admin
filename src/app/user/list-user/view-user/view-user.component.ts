import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../auth.service';
import {DateService} from '../../../date.service';
import {UserModel} from '../../user.model';
import {UserService} from '../../user.service';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../../file.service';
import {PointService} from '../../../point/point.service';
import {PointModel} from '../../../point/point.model';

@Component({
  selector: 'th-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss'],
  providers: [AuthService, DateService, UserService, FileService, PointService]
})
export class ViewUserComponent implements OnInit {

  model: UserModel = new UserModel();

  totalPoint: number = 0;

  pointHistory: PointModel[] = [];

  listPointModal: boolean = false;

  constructor(private _authService: AuthService,
              private _dateService: DateService,
              private _activatedRoute: ActivatedRoute,
              private _userService: UserService,
              private _fileService: FileService,
              private _pointService: PointService) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this._userService.get(params['id'], this._authService.getToken()).subscribe(response => {
          this.model = response.model;
        });

        this._pointService.getTotalPoint(params['id'], this._authService.getToken()).subscribe(response => {
          if (response.model) this.totalPoint = response.model.point;
        });

        // this._pointService.listUserPointHistory(params['id'], this._authService.getToken()).subscribe(response => {
        //   this.pointHistory = response.models;
        // });
      }
    });
  }


  getStringDate(date) {
    return this._dateService.getDateNumberFormat(date);
  }

  getFilePath(fileId: string) {
    return this._fileService.generateImagePathById(fileId);
  }

  setListPointModal(val: boolean) {
    this.listPointModal = val;
  }

}
