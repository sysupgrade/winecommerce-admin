import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterUserListComponent } from './filter-user-list.component';

describe('FilterUserListComponent', () => {
  let component: FilterUserListComponent;
  let fixture: ComponentFixture<FilterUserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterUserListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterUserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
