import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'th-filter-user-list',
  templateUrl: './filter-user-list.component.html',
  styleUrls: ['./filter-user-list.component.scss']
})
export class FilterUserListComponent implements OnInit {

  search : string;

  @Output()
  filter : EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  handleOnFilter(){
    this.filter.emit(this.search)
  }

  reset(){
    this.filter.emit('');
    this.search = null;
  }

}
