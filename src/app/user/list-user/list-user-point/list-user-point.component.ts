import {Component, Input, OnInit} from '@angular/core';
import {PointModel} from '../../../point/point.model';
import {PointService} from '../../../point/point.service';
import {AuthService} from '../../../auth.service';
import {ActivatedRoute} from '@angular/router';
import {DateService} from '../../../date.service';

@Component({
  selector: 'th-list-user-point',
  templateUrl: './list-user-point.component.html',
  styleUrls: ['./list-user-point.component.scss']
})
export class ListUserPointComponent implements OnInit {

  @Input() page: number = 1;

  @Input() pageSize: number = 10;

  models: PointModel[]=[];

  userId : string;

  total : number = 0;

  constructor(private _pointService : PointService,
              private _authService : AuthService,
              private _activatedRoute : ActivatedRoute,
              public _dateService : DateService) { }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params=> this.userId=params['id']);
    this.getPage(this.page);
  }

  getPage(page){
    this.page = page;
    this._pointService.listUserPointHistory(this.page, this.pageSize, this.userId, this._authService.getToken()).subscribe(response => {
      this.models = response.models;
      this.total = response.total;
    });
  }

}
