import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUserPointComponent } from './list-user-point.component';

describe('ListUserPointComponent', () => {
  let component: ListUserPointComponent;
  let fixture: ComponentFixture<ListUserPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUserPointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUserPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
