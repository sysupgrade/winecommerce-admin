import {Component, Input, OnInit} from '@angular/core';
import {PointModel} from '../../../point/point.model';
import {PointService} from '../../../point/point.service';
import {AuthService} from '../../../auth.service';
import {ActivatedRoute} from '@angular/router';
import {DateService} from '../../../date.service';
import {SalesService} from '../../../sales/sales.service';
import {SalesModel} from '../../../sales/sales.model';
import {MANUAL_BANK_TRANSFER_PAYMENT_ID, PAYPAL_PAYMENT_ID} from '../../../models/payment.model';

@Component({
  selector: 'th-list-order-by-user',
  templateUrl: './list-order-by-user.component.html',
  styleUrls: ['./list-order-by-user.component.scss'],
  providers:[SalesService, AuthService, DateService]
})
export class ListOrderByUserComponent implements OnInit {

  @Input() page: number = 1;

  @Input() pageSize: number = 10;

  models: SalesModel[] = [];

  customerId: string;

  total: number = 0;

  constructor(private _authService: AuthService,
              private _activatedRoute: ActivatedRoute,
              public _dateService: DateService,
              private _salesService: SalesService) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => this.customerId = params['id']);
    this.getPage(this.page);
  }

  getPage(page) {
    this.page = page;
    this._salesService.listOrderByUser(this._authService.getToken(), page, this.pageSize, this.customerId).subscribe(response => {
      this.models = response.models;
      this.total = response.total;
    });


  }


  getPaymentMethodText(paymentMethodId: number) {
    if (paymentMethodId == MANUAL_BANK_TRANSFER_PAYMENT_ID) {
      return "Bank Transfer";
    }
    else if (paymentMethodId == PAYPAL_PAYMENT_ID) {
      return "Paypal";
    }

  }
}
