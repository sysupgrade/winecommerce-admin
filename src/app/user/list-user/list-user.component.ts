import { DateService } from './../../date.service';
import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../user.service';
import { UserModel, UserResponse, UserCsvModel } from '../user.model';
import { AuthService } from '../../auth.service';
import { Angular2Csv } from 'angular2-csv';
import {Router} from '@angular/router';

@Component({
  selector: 'th-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss'],
  providers: [UserService, DateService]
})
export class ListUserComponent implements OnInit {

  @Input() page: number = 1;

  @Input() pageSize: number = 10;

  models: UserModel[] = [];

  total: number = 0;

  search : string = '';

  constructor(private _userService : UserService,
      private _dateService : DateService,
      private _authService : AuthService,
              private _router : Router) { }

  ngOnInit() {
    this.getPage(this.page);
  }

  getPage(page : number) {
    this.page = page;
    this._userService.list(this.page, this.pageSize,
        this._authService.getToken(), this.search)
      .subscribe((response : UserResponse) => {
      this.models = response.models;
      this.total = response.total;
    });
  }

  getDateTime(timestamp : number) : string {
    return this._dateService.getDateTime(timestamp + "");
  }

  downloadCsv() {
    this._userService.list(1, -1, this._authService.getToken(), this.search)
        .subscribe(this.handleListUserForCsv.bind(this));
  }

  handleListUserForCsv(response : UserResponse) {
    let csvModels : UserCsvModel[] = [];
    response.models.forEach((model) => {
      let csvModel : UserCsvModel = new UserCsvModel;
      csvModel.id = model.id + "";
      csvModel.firstName = model.firstName;
      csvModel.lastName = model.lastName;
      csvModel.email = model.email;
      csvModel.createdAt = this.getDateTime(model.createdAt);
      csvModels.push(csvModel);
    });
    let title : string = "User List";
    let options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true,
      headers: ['ID', 'FIRST NAME', 'LAST NAME', 'EMAIL', 'JOINED AT'],
      title: [title],
      margeTitle: true
    };
    new Angular2Csv(JSON.stringify(csvModels), title, options);

  }

  handleSearch(event : string){
    this.search = event;
    this.getPage(this.page);
  }

  getUserURL(userId : string){
    this._router.navigate(['pages/user', userId]);
  }
}
