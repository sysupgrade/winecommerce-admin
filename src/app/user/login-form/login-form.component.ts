import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './../../auth.service';
import { LoginModel, LoginResponse, ApiUser } from './../user.model';
import { Component, OnInit } from '@angular/core';
import {AppSettings} from '../../app.setting';

@Component({
  selector: 'az-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  providers: [AuthService]
})
export class LoginFormComponent implements OnInit {
  static readonly EMAIL : string = "email";
  static readonly PASSWORD : string = "password";

  model : LoginModel;

  url : string = AppSettings.USER_LOGIN;

  constructor(private authService : AuthService, private router : Router) { }

  ngOnInit() {
    this.model = new LoginModel;
  }

  getRules() : Object {
      let json : Object = {};
      json[LoginFormComponent.EMAIL] = [this.model.email, [Validators.required]];
      json[LoginFormComponent.PASSWORD] = [this.model.password, [Validators.required]];
      return json;
  }

  getValidationMessages() : Object {
      let json : Object = {};
      json[LoginFormComponent.EMAIL] = {"required" : "Email is required"};
      json[LoginFormComponent.PASSWORD] = {"required" : "Password is required"};
      return json;
  }

  successForm(object : LoginResponse) {
    this.authService.login(object.authKey, object.firstName, object.lastName);
    this.router.navigate([''])
  }
}
