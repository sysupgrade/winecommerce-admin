import {RouterModule} from '@angular/router';
import {ToolkitModule} from './../toolkit/toolkit.module';
import {HttpModule} from '@angular/http';
import {LoginFormComponent} from './login-form/login-form.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ToolkitModule,
    RouterModule
  ],
  declarations: [LoginFormComponent],
  exports: [LoginFormComponent]
})
export class UserModule { }
