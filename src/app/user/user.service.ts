import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {UserInfoResponse, UserResponse} from './user.model';
import {AppSettings} from '../app.setting';

@Injectable()
export class UserService {

  constructor(private _http : Http) { }

  get(userId : string, token : string) : Observable<UserResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + token
    });
    let options = new RequestOptions({ headers: headers });

    let params: URLSearchParams = new URLSearchParams();
    params.append("id", userId);

    return this._http.get(AppSettings.USER_GET_BY_ID + "?" + params.toString(), options)
      .map((response) => response.json());
  }

  list(page : number, pageSize : number, token : string, search) : Observable<UserResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + token
    });
    let options = new RequestOptions({ headers: headers });

    let params: URLSearchParams = new URLSearchParams();
    params.append("page", page + "");
    params.append("pageSize", pageSize + "");
    params.append("search", search);

    return this._http.get(AppSettings.USER_LIST + "?" + params.toString(), options)
      .map((response) => response.json());

  }
}
