import {Component, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../file.service';
import {ShipmentModel} from '../shipment.model';
import {ShipmentService} from '../shipment.service';

@Component({
  selector: 'th-shipment-view',
  templateUrl: './shipment-view.component.html',
  styleUrls: ['./shipment-view.component.scss'],
  providers: [ShipmentService, FileService]
})
export class ShipmentViewComponent implements OnInit {

  shipmentMethodId: string;

  @Output()
  model: ShipmentModel;

  isEditMode: boolean = false;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _shipmentService: ShipmentService,
    private _fileService: FileService
  ) {
  }

  ngOnInit() {
    this.model = new ShipmentModel();
    this._activatedRoute.params.subscribe(params => {
      this.shipmentMethodId = params['id'];
      this.getData();
    });
  }

  getData() {
    this._shipmentService.get(this.shipmentMethodId)
      .subscribe((response) => {
        this.model = response.model;
      });
  }

  setEditModel(val: boolean) {
    this.isEditMode = val;
  }


}
