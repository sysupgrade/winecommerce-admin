import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShipmentEditFormComponent} from './shipment-edit-form.component';

describe('ShipmentEditFormComponent', () => {
  let component: ShipmentEditFormComponent;
  let fixture: ComponentFixture<ShipmentEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShipmentEditFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmentEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
