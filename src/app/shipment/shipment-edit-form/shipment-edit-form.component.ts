import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Validators} from '@angular/forms';
import {ProductResponse} from '../../product/product.model';
import {AppSettings} from '../../app.setting';
import {ShipmentModel} from '../shipment.model';

@Component({
  selector: 'th-shipment-edit-form',
  templateUrl: './shipment-edit-form.component.html',
  styleUrls: ['./shipment-edit-form.component.scss']
})
export class ShipmentEditFormComponent implements OnInit {

  readonly ID: string = 'id';

  readonly NAME: string = 'name';

  readonly COST: string = 'cost';

  url: string = AppSettings.SHIPMENT_METHOD_EDIT;

  @Input()
  model: ShipmentModel = new ShipmentModel();

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.model.name, [Validators.required, Validators.minLength(3)]];
    json[this.COST] = [this.model.cost, Validators.required];
    json[this.ID] = [this.model.id];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'product name is required'};
    json[this.COST] = {'required' : 'cost is required'};
    json[this.ID] = {'required': 'id is required'};
    return json;
  }

  successForm(object: ProductResponse) {
    if (!object) return;
    this.success.emit(true);
    this.model = new ShipmentModel();
  }
}
