export class ShipmentModel {
  id: string;
  name: string;
  cost: string;
  createdAt: string;
  updatedAt: string;
  status: string;
}

export class ShipmentResponse {
  model: ShipmentModel;
  models: ShipmentModel[];
  status: string;
}

export class SHIPMENT_URL {
  public static ROOT = '/pages/shipment/';
}
