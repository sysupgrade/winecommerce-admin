import {Component, Input, OnInit} from '@angular/core';
import {DateService} from '../../date.service';
import {Router} from '@angular/router';
import {ShipmentService} from '../shipment.service';
import {SHIPMENT_URL, ShipmentModel} from '../shipment.model';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'th-shipment-list',
  templateUrl: './shipment-list.component.html',
  styleUrls: ['./shipment-list.component.scss'],
  providers: [ShipmentService, DateService, AuthService]
})
export class ShipmentListComponent implements OnInit {


  models: ShipmentModel[] = [];

  editModal : boolean = false;

  confirmDeleteModal : boolean = false;

  shipmentModel : ShipmentModel = new ShipmentModel();

  constructor(
    private _shipmentService: ShipmentService,
    private _router: Router,
    private _dateService: DateService,
    private _auhtService : AuthService
  ) {
  }

  ngOnInit() {
    this.list();
  }

  list() {
    this._shipmentService.list().subscribe((response) => {
        this.models = response.models;
      });

    this.editModal = false;
  }

  getShipmentURL(id: string) {
    this._router.navigate([SHIPMENT_URL.ROOT, id])
  }

  getDate(date: string): string {
    return this._dateService.getDateNumberFormat(date);
  }

  setEditModal(editModal : boolean, model : ShipmentModel){
    this.editModal = editModal;
    if (editModal) {
      this.shipmentModel =  model;
    }
  }

  delete(){
    this._shipmentService.changeStatus(this._auhtService.getToken(), this.shipmentModel, '0')
      .subscribe(response=>{
        if (response.status){
          this.list();
        }
      });
  }

  setConfirDeleteModal(confirmDeleteModal : boolean, shipmentModel : ShipmentModel){
    this.confirmDeleteModal = true;
    this.shipmentModel = shipmentModel;
  }
}

