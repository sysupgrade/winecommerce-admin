import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../app.setting';
import {ShipmentModel, ShipmentResponse} from './shipment.model';

@Injectable()
export class ShipmentService {

  constructor(private _http: Http) {
  }

  list(): Observable<ShipmentResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();

    return this._http.get(AppSettings.SHIPMENT_METHOD_LIST + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  get(productId: string): Observable<ShipmentResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', productId);

    return this._http.get(AppSettings.SHIPMENT_METHOD_GET + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  changeStatus(token : string, shipmentModel : ShipmentModel, status : string): Observable<ShipmentResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '+token
    });

    let options = new RequestOptions({headers: headers});


    let params: URLSearchParams = new URLSearchParams();
    params.append('id', shipmentModel.id);
    params.append('status', status);

    return this._http.post(AppSettings.SHIPMENT_METHOD_STATUS, params, options)
      .map((response) => response.json());
  }

}
