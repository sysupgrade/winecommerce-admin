import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ShipmentViewComponent} from './shipment-view/shipment-view.component';
import {ShipmentAddFormComponent} from './shipment-add-form/shipment-add-form.component';
import {ShipmentListComponent} from './shipment-list/shipment-list.component';
import {ShipmentEditFormComponent} from './shipment-edit-form/shipment-edit-form.component';
import {RouterModule} from '@angular/router';
import {DirectivesModule} from '../theme/directives/directives.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ToolkitModule} from '../toolkit/toolkit.module';

export const routes = [
  {path: '', component: ShipmentListComponent},
  {path: 'add', component: ShipmentAddFormComponent},
  {path: 'edit', component: ShipmentEditFormComponent},
  {path: ':id', component: ShipmentViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    DirectivesModule,
    RouterModule.forChild(routes),
  ],

  declarations: [ShipmentViewComponent,
    ShipmentAddFormComponent,
    ShipmentListComponent,
    ShipmentEditFormComponent],
  exports: [ShipmentViewComponent,
    ShipmentAddFormComponent,
    ShipmentListComponent,
    ShipmentEditFormComponent]
})
export class ShipmentModule {
}
