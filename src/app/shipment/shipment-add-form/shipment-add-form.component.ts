import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Validators} from '@angular/forms';
import {ProductResponse} from '../../product/product.model';
import {AppSettings} from '../../app.setting';
import {ShipmentModel} from '../shipment.model';

@Component({
  selector: 'th-shipment-add-form',
  templateUrl: './shipment-add-form.component.html',
  styleUrls: ['./shipment-add-form.component.scss']
})
export class ShipmentAddFormComponent implements OnInit {

  readonly NAME: string = 'name';

  readonly COST: string = 'cost';

  url: string = AppSettings.SHIPMENT_METHOD_ADD;

  @Input()
  model: ShipmentModel = new ShipmentModel();

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.model.name, [Validators.required, Validators.minLength(3)]];
    json[this.COST] = [this.model.cost, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'name is required'};
    json[this.COST] = {'required': 'cost is required'};
    return json;
  }

  successForm(object: ProductResponse) {
    if (!object) return;
    this.success.emit(true);
    this.model = new ShipmentModel();
  }
}
