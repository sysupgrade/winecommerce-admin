import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShipmentAddFormComponent} from './shipment-add-form.component';

describe('ShipmentAddFormComponent', () => {
  let component: ShipmentAddFormComponent;
  let fixture: ComponentFixture<ShipmentAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShipmentAddFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmentAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
