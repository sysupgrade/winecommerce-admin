import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPageComponent } from './user-page.component';
import { RouterModule } from '@angular/router';
import { ListUserModule } from '../../user/list-user/list-user.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { DirectivesModule } from '../../theme/directives/directives.module';
import {ViewUserPageComponent} from './view-user-page/view-user-page.component';


export const routes = [
  { path: '', component: UserPageComponent},
  { path: ':id', component: ViewUserPageComponent},
];

@NgModule({
  imports: [
    CommonModule, ListUserModule, RouterModule.forChild(routes),
    PipesModule, DirectivesModule
  ],
  declarations: [UserPageComponent, ViewUserPageComponent]
})
export class UserPageModule { }
