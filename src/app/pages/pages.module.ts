import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ToastrModule} from 'ngx-toastr';
import {DirectivesModule} from '../theme/directives/directives.module';
import {PipesModule} from '../theme/pipes/pipes.module';
import {routing} from './pages.routing';
import {PagesComponent} from './pages.component';
import {MenuComponent} from '../theme/components/menu/menu.component';
import {SidebarComponent} from '../theme/components/sidebar/sidebar.component';
import {NavbarComponent} from '../theme/components/navbar/navbar.component';
import {MessagesComponent} from '../theme/components/messages/messages.component';
import {BreadcrumbComponent} from '../theme/components/breadcrumb/breadcrumb.component';
import {BackTopComponent} from '../theme/components/back-top/back-top.component';

const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  imports: [
    CommonModule,
    PerfectScrollbarModule.forRoot(PERFECT_SCROLLBAR_CONFIG),
    ToastrModule.forRoot(),
    DirectivesModule,
    PipesModule,
    routing
  ],
  declarations: [
    PagesComponent,
    MenuComponent,
    SidebarComponent,
    NavbarComponent,
    MessagesComponent,
    BreadcrumbComponent,
    BackTopComponent
  ]
})
export class PagesModule { }
