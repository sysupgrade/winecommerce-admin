import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {PagesComponent} from './pages.component';

export const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        redirectTo: '/pages/sales',
        pathMatch: 'full'
      },
      // {
      //   path: 'dashboard',
      //   loadChildren: 'app/pages/dashboard/dashboard.module#DashboardModule',
      //   data: {breadcrumb: 'Dashboard'}
      // },
      {
        path: 'user',
        loadChildren: 'app/pages/user-page/user-page.module#UserPageModule',
        data: {breadcrumb: 'User'}
      },
      {
        path: 'product',
        loadChildren: 'app/product/product.module#ProductModule',
        data: {breadcrumb: 'Product'}
      },
      {
        path: 'attribute',
        loadChildren: 'app/attribute/attribute.module#AttributeModule',
        data: {breadcrumb: 'Attribute'}
      },
      {
        path: 'category',
        loadChildren: 'app/category/category.module#CategoryModule',
        data: {breadcrumb: 'Category'}
      },
      // {
      //   path: 'feedback',
      //   loadChildren: 'app/feedback/feedback.module#FeedbackModule',
      //   data: {breadcrumb: 'Feedback'}
      // },
      {
        path: 'sales',
        loadChildren: 'app/sales/sales.module#SalesModule',
        data: {breadcrumb: 'Sales'}
      },
      {
        path: 'shipment',
        loadChildren: 'app/shipment/shipment.module#ShipmentModule',
        data: {breadcrumb: 'Shipment'}
      },
      {
        path: 'salesrule',
        loadChildren: 'app/salesrule/salesrule.module#SalesruleModule',
        data: {breadcrumb: 'Coupon'}
      },
      {
        path: 'news',
        loadChildren: 'app/blogs/blog/blog.module#BlogModule',
        data: {breadcrumb: 'News'}
      },
      {
        path: 'contact',
        loadChildren: 'app/blogs/contact/contact.module#ContactModule',
        data: {breadcrumb: 'Contact'}
      },
      {
        path: 'event',
        loadChildren: 'app/blogs/event/event.module#EventModule',
        data: {breadcrumb: 'Event'}
      },
      {
        path: 'newsletter',
        loadChildren: 'app/blogs/newsletter/newsletter.module#NewsletterModule',
        data: {breadcrumb: 'Newsletter'}
      },
      {
        path: 'testimonial',
        loadChildren: 'app/blogs/testimonial/testimonial.module#TestimonialModule',
        data: {breadcrumb: 'Testimonial'}
      },
      {
        path: 'report',
        loadChildren: 'app/report/report.module#ReportModule',
        data: {breadcrumb: 'Report'}
      },
      {
        path: 'invoice',
        loadChildren: 'app/sales/invoice/invoice.module#InvoiceModule',
        data: {breadcrumb: 'Invoice'}
      },
      {
        path: 'subscription',
        loadChildren: 'app/subscribe/subscribe.module#SubscribeModule',
        data: {breadcrumb: 'Subscription'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
