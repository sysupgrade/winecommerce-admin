export class CategoryModel {
  id: string;
  name: string;
  description: string;
  createdAt: string;
  updatedAt: string;
}

export class CategoryResponse {
  model: CategoryModel;
  models: CategoryModel[] = [];
  total: number;
  status: string;
}


export class CATEGORY_URL {
  public static ROOT = '/pages/category/';
}
