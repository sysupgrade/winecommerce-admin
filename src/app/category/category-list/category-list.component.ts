import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ATTRIBUTE_URL} from '../../attribute/attribute.model';
import {CategoryService} from '../category.service';
import {CategoryModel} from '../category.model';
import {DateService} from '../../date.service';

@Component({
  selector: 'th-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
  providers: [CategoryService, DateService]
})
export class CategoryListComponent implements OnInit {


  @Input()
  page: number = 1;

  @Input()
  pageSize: number = 10;

  status: string;

  search: string;

  models: CategoryModel[] = [];

  total: number = 0;

  isAddCategory: boolean = false;

  constructor(
    private _categoryService: CategoryService,
    private _router: Router,
    private _dateService : DateService
  ) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._categoryService.list(this.page, this.pageSize, this.status, this.search)
      .subscribe((response) => {
        this.models = response.models;
        this.total = response.total;
      });
  }

  getProductURL(id: string) {
    this._router.navigate([ATTRIBUTE_URL.ROOT, id])
  }

  setAddCategory(val: boolean) {
    this.isAddCategory = val;
    if (val == false) {
      this.list(this.page);
    }
  }


  getDate(date: string): string {
    return this._dateService.getDateNumberFormat(date);
  }
}

