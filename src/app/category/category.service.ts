import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../app.setting';
import {CategoryResponse} from './category.model';

@Injectable()
export class CategoryService {

  constructor(private _http: Http) {
  }

  list(page: number, pageSize: number, status: string, search: string): Observable<CategoryResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');
    if (status) params.append('status', status);
    if (search) params.append('q', search);

    return this._http.get(AppSettings.CATEGORY_LIST + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  get(productId: string): Observable<CategoryResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', productId);

    return this._http.get(AppSettings.CATEGORY_GET + '?' + params.toString(), options)
      .map((response) => response.json());
  }

}
