import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoryListComponent} from './category-list/category-list.component';
import {CategoryViewComponent} from './category-view/category-view.component';
import {CategoryEditFormComponent} from './category-edit-form/category-edit-form.component';
import {CategoryAddFormComponent} from './category-add-form/category-add-form.component';
import {DirectivesModule} from '../theme/directives/directives.module';
import {RouterModule} from '@angular/router';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ToolkitModule} from '../toolkit/toolkit.module';

export const routes = [
  {path: '', component: CategoryListComponent},
  {path: 'add', component: CategoryAddFormComponent},
  {path: 'edit', component: CategoryEditFormComponent},
  {path: ':id', component: CategoryViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    DirectivesModule,
    RouterModule.forChild(routes),
  ],
  declarations: [CategoryListComponent, CategoryViewComponent, CategoryAddFormComponent, CategoryEditFormComponent],
  exports: [CategoryListComponent, CategoryViewComponent, CategoryAddFormComponent, CategoryEditFormComponent],
})
export class CategoryModule {
}
