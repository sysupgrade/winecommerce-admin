import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductResponse} from '../../product/product.model';
import {Validators} from '@angular/forms';
import {AppSettings} from '../../app.setting';
import {CategoryModel} from '../category.model';

@Component({
  selector: 'th-category-add-form',
  templateUrl: './category-add-form.component.html',
  styleUrls: ['./category-add-form.component.scss']
})
export class CategoryAddFormComponent implements OnInit {

  readonly NAME: string = 'name';

  readonly DESCRIPTION: string = 'description';

  url: string = AppSettings.CATEGORY_ADD;

  @Input()
  model: CategoryModel = new CategoryModel();

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.model.name, [Validators.required, Validators.minLength(3)]];
    json[this.DESCRIPTION] = [this.model.description];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'name is required'};
    json[this.DESCRIPTION] = {};
    return json;
  }

  successForm(object: ProductResponse) {
    if (!object) return;
    this.success.emit(true);
    this.model = new CategoryModel();
  }
}
