import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { ContactResponse } from '../../models/contact.model';

@Injectable()
export class ContactService {

  constructor(private _http : Http) { }

  list(token : string, page : number, pageSize : number) : Observable<ContactResponse> {
    let headers : Headers = new Headers();
    headers.append("Authorization", "Bearer " + token);
    let params : URLSearchParams = new URLSearchParams;
    params.append("page", page + "");
    params.append("pageSize", pageSize + "");
    return this._http.get("./api/blog/contact/list?" + params.toString(), { headers: headers})
              .map((response : Response) => response.json());
  }
}
