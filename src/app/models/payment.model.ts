export function parsePaymentMethodToText(paymentMethodId : number) : string {
    if(PAYPAL_PAYMENT_ID == paymentMethodId) {
        return PAYPAL_PAYMENT;
    }
    else if(MANUAL_BANK_TRANSFER_PAYMENT_ID == paymentMethodId) {
        return MANUAL_BANK_TRANSFER_PAYMENT;
    }

    return "";
}

export const PAYPAL_PAYMENT_ID : number = 2;

export const MANUAL_BANK_TRANSFER_PAYMENT_ID : number = 1;

export const PAYPAL_PAYMENT : string = "Paypal";

export const MANUAL_BANK_TRANSFER_PAYMENT : string = "Transfer Bank";