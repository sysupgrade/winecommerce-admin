export class ContactModel {
    id : number;
    title : string;
    message : string;
    email : string;
    eventId : string;
    phoneNumber : string;
    createdAt : number;
    updatedAt : number;
}

export class ContactResponse {
    status : number;
    model : ContactModel;
    models : ContactModel[];
    total : number;
}
