import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestimonialViewComponent} from './testimonial-view/testimonial-view.component';
import {TestimonialListComponent} from './testimonial-list/testimonial-list.component';
import {TestimonialEditFormComponent} from './testimonial-edit-form/testimonial-edit-form.component';
import {TestimonialAddFormComponent} from './testimonial-add-form/testimonial-add-form.component';
import {PipesModule} from '../../theme/pipes/pipes.module';
import {RouterModule} from '@angular/router';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {DirectivesModule} from '../../theme/directives/directives.module';
import { TestimonialService } from './testimonial.service';

export const routes = [
  {path: '', component: TestimonialListComponent},
  {path: 'add', component: TestimonialAddFormComponent},
  {path: 'edit', component: TestimonialEditFormComponent},
  {
    path: 'view/:id',
    component: TestimonialViewComponent
  },
  {path: ':id', component: TestimonialViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    DirectivesModule,
    PipesModule,
    RouterModule.forChild(routes),
  ],

  declarations: [TestimonialAddFormComponent, TestimonialEditFormComponent, TestimonialListComponent, TestimonialViewComponent],
  exports: [TestimonialAddFormComponent, TestimonialEditFormComponent, TestimonialListComponent, TestimonialViewComponent],
  providers: [TestimonialService]
})
export class TestimonialModule {
}
