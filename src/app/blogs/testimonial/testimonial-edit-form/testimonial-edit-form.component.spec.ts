import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TestimonialEditFormComponent} from './testimonial-edit-form.component';

describe('TestimonialEditFormComponent', () => {
  let component: TestimonialEditFormComponent;
  let fixture: ComponentFixture<TestimonialEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestimonialEditFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestimonialEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
