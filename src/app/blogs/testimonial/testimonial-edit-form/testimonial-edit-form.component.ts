import {Component, OnInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import { TestimonialModel, TestimonialResponse } from '../testimonial.model';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TestimonialService } from '../testimonial.service';
import { AppSettings } from '../../../app.setting';

@Component({
  selector: 'th-testimonial-edit-form',
  templateUrl: './testimonial-edit-form.component.html',
  styleUrls: ['./testimonial-edit-form.component.scss']
})
export class TestimonialEditFormComponent implements OnInit {

  readonly ID : string = "id";

  readonly JOB_TITLE: string = 'jobTitle';

  readonly PERSON_NAME: string = 'personName';

  readonly MESSAGE: string = 'message';

  readonly PERSONAL_NAME_CHINESE: string = 'personNameChinese';
  readonly JOB_TITLE_CHINESE: string = 'jobTitleChinese';
  readonly MESSAGE_CHINESE: string = 'messageChinese';

  @Input()
  model: TestimonialModel = new TestimonialModel();

  @Output()
  success : EventEmitter<any> = new EventEmitter<any>();

  url: string = AppSettings.TESTIMONIAL_EDIT;

  constructor() {
  }

  ngOnInit() {

  }

  getRules(): Object {
    let json: Object = {};
    json[this.ID] = [this.model.id, [Validators.required]];
    json[this.JOB_TITLE] = [this.model.jobTitle,
      [Validators.required, Validators.minLength(3)]];
    json[this.PERSON_NAME] = [this.model.personName, [Validators.required]];
    json[this.MESSAGE] = [this.model.message, [Validators.required]];
    json[this.PERSONAL_NAME_CHINESE] = [this.model.personNameChinese, [Validators.required]];
    json[this.JOB_TITLE_CHINESE] = [this.model.jobTitleChinese, [Validators.required]];
    json[this.MESSAGE_CHINESE] = [this.model.messageChinese, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.ID] = { "required": "Id is required"};
    json[this.JOB_TITLE] = {'required': 'Job Title is required'};
    json[this.PERSON_NAME] = {'required': 'Person Name is required'};
    json[this.MESSAGE] = {'required': 'Message is required'};
    json[this.PERSONAL_NAME_CHINESE] = {'required': 'required'};
    json[this.JOB_TITLE_CHINESE] = {'required': 'required'};
    json[this.MESSAGE_CHINESE] = {'required': 'required'};
    return json;
  }

  successForm(object: TestimonialResponse) {
    if (!object.status) {
      return;
    }
    this.success.emit(null);
  }

}
