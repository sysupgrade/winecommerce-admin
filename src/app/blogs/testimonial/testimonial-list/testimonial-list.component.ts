import {Component, Input, OnInit} from '@angular/core';
import {TestimonialService} from '../testimonial.service';
import {DateService} from '../../../date.service';
import {TestimonialModel, TestimonialResponse} from '../testimonial.model';
import {Router} from '@angular/router';
import {AuthService} from '../../../auth.service';

@Component({
  selector: 'th-testimonial-list',
  templateUrl: './testimonial-list.component.html',
  styleUrls: ['./testimonial-list.component.scss'],
  providers: [TestimonialService, DateService]
})
export class TestimonialListComponent implements OnInit {

  @Input() page: number = 1;

  @Input() pageSize: number = 10;

  data: TestimonialModel[] = [];

  total: number = 0;

  constructor(private _testimonialService: TestimonialService,
              private _dateService: DateService,
              private _router: Router,
              private _authService: AuthService) {
  }

  ngOnInit() {
    this.getPage(this.page);
  }

  getPage(page: number) {
    this.page = page;
    this._testimonialService.list(this.page, this.pageSize, this._authService.getToken())
      .subscribe(this.handleListResponse.bind(this));
  }

  handleListResponse(response: TestimonialResponse) {
    this.data = response.models;
    this.total = response.total;
  }

  getDateTime(timestamp: string): string {
    return this._dateService.getDateTime(timestamp);
  }

  redirectToTestimonialUrl(testimonial: TestimonialModel) {
    this._router.navigate([this._testimonialService.getViewTestimonialUrl(testimonial.id)]);
  }

}
