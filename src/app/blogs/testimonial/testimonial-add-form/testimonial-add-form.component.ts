import {Component, OnInit} from '@angular/core';
import {TestimonialService} from '../testimonial.service';
import {FileModel} from '../../../toolkit/toolkit.model';
import {TestimonialModel, TestimonialResponse} from '../testimonial.model';
import {Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AppSettings} from '../../../app.setting';

@Component({
  selector: 'th-testimonial-add-form',
  templateUrl: './testimonial-add-form.component.html',
  styleUrls: ['./testimonial-add-form.component.scss'],
  providers: [TestimonialService]
})
export class TestimonialAddFormComponent implements OnInit {

  readonly JOB_TITLE: string = 'jobTitle';

  readonly PERSON_NAME: string = 'personName';

  readonly PERSON_PICTURE_ID: string = 'personPictureId';

  readonly MESSAGE: string = 'message';

  readonly PERSONAL_NAME_CHINESE: string = 'personNameChinese';
  readonly JOB_TITLE_CHINESE: string = 'jobTitleChinese';
  readonly MESSAGE_CHINESE: string = 'messageChinese';

  model: TestimonialModel = new TestimonialModel();

  url: string = AppSettings.TESTIMONIAL_ADD;

  constructor(private _testimonialService: TestimonialService,
              private _router: Router) {
  }

  ngOnInit() {

  }

  getRules(): Object {
    let json: Object = {};
    json[this.JOB_TITLE] = [this.model.jobTitle,
      [Validators.required, Validators.minLength(3)]];
    json[this.PERSON_NAME] = [this.model.personName, [Validators.required]];
    json[this.PERSON_PICTURE_ID] = [this.model.personPictureId, [Validators.required]];
    json[this.MESSAGE] = [this.model.message, [Validators.required]];
    json[this.PERSONAL_NAME_CHINESE] = [this.model.personNameChinese, [Validators.required]];
    json[this.JOB_TITLE_CHINESE] = [this.model.jobTitleChinese, [Validators.required]];
    json[this.MESSAGE_CHINESE] = [this.model.messageChinese, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.JOB_TITLE] = {'required': 'Job Title is required'};
    json[this.PERSON_NAME] = {'required': 'Person Name is required'};
    json[this.PERSON_PICTURE_ID] = {'required': 'Person Picture is required'};
    json[this.MESSAGE] = {'required': 'Message is required'};
    json[this.PERSONAL_NAME_CHINESE] = {'required': 'required'};
    json[this.JOB_TITLE_CHINESE] = {'required': 'required'};
    json[this.MESSAGE_CHINESE] = {'required': 'required'};
    return json;
  }

  successForm(object: TestimonialResponse) {
    if (!object.status) {
      return;
    }
    this._router.navigate([this._testimonialService.getViewTestimonialUrl(object.model.id)]);
    this.model = new TestimonialModel();
  }

  handleOnImageSelect(imageIds: FileModel[]) {
    if (imageIds.length == 0) return;
    this.model.personPictureId = imageIds[0].id;
  }
}
