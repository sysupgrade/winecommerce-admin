import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TestimonialAddFormComponent} from './testimonial-add-form.component';

describe('TestimonialAddFormComponent', () => {
  let component: TestimonialAddFormComponent;
  let fixture: ComponentFixture<TestimonialAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestimonialAddFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestimonialAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
