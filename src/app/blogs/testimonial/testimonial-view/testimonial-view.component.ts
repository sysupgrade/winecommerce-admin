import {Component, OnInit} from '@angular/core';
import {TestimonialModel, TestimonialResponse} from '../testimonial.model';
import {FileService} from '../../../file.service';
import {DateService} from '../../../date.service';
import {TestimonialService} from '../testimonial.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {AuthService} from '../../../auth.service';

@Component({
  selector: 'th-testimonial-view',
  templateUrl: './testimonial-view.component.html',
  styleUrls: ['./testimonial-view.component.scss'],
  providers: [TestimonialService, DateService, FileService]
})
export class TestimonialViewComponent implements OnInit {

  testimonial: TestimonialModel = new TestimonialModel();

  isEditMode: boolean = false;

  testimonialId: number = null;

  isDeleteConfirmDialogOpened: boolean = false;

  constructor(private _testimonialService: TestimonialService,
              private _dateService: DateService,
              private _fileService: FileService,
              private _authService: AuthService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe((params: Params) => {
      this.testimonialId = params['id'];
      this.getData();
    });
  }

  getData() {
    this._testimonialService.getById(this.testimonialId, this._authService.getToken())
      .subscribe((response: TestimonialResponse) => {
        if (!response.status) return;
        this.testimonial = response.model;
      });
  }

  getDateTime(timestamp: string): string {
    return this._dateService.getDateTime(timestamp);
  }

  setEditMode(editMode: boolean) {
    this.isEditMode = editMode;
  }

  generateImagePathById(imageId: string): string {
    return this._fileService.generateImagePathById(imageId);
  }

  handleEditTestimonialSuccess() {
    this.getData();
    this.isEditMode = false;
  }

  publish() {
    this._testimonialService.changeStatus(this.testimonialId, this._authService.getToken(), TestimonialModel.STATUS_ACTIVE)
      .subscribe((response: TestimonialResponse) => this.getData());
  }

  deactivate() {
    this._testimonialService.changeStatus(this.testimonialId, this._authService.getToken(), TestimonialModel.STATUS_PENDING)
      .subscribe((response: TestimonialResponse) => this.getData());

  }

  delete() {
    this._testimonialService.changeStatus(this.testimonialId, this._authService.getToken(), TestimonialModel.STATUS_DELETE)
      .subscribe((response: TestimonialResponse) => {
        this.getData();
        this._router.navigate([this._testimonialService.getListTestimonialUrl()]);

      });
  }

  showDeleteDialog() {
    this.isDeleteConfirmDialogOpened = true;
  }
}
