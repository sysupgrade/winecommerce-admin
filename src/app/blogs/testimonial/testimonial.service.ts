import {TestimonialResponse} from './testimonial.model';
import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../../app.setting';

@Injectable()
export class TestimonialService {

  constructor(private _http: Http) {
  }


  getListTestimonialUrl(): string {
    return '/pages/testimonial/list';
  }

  getAddTestimonialUrl(): string {
    return '/pages/testimonial/add';
  }

  getViewTestimonialUrl(testimonialId: string): string {
    return '/pages/testimonial/view/' + testimonialId;
  }


  list(page: number, pageSize: number, token: string): Observable<TestimonialResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');

    return this._http.get(AppSettings.TESTIMONIAL_LIST + '?' + params.toString(), options)
      .map((response) => response.json());
  }


  getById(testimonialId: number, token: string): Observable<TestimonialResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    return this._http.get(AppSettings.TESTIMONIAL_GET_BY_ID + '?id=' + testimonialId, options)
      .map((response) => response.json());
  }

  changeStatus(testimonialId: number, token: string, status: string): Observable<TestimonialResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params = new URLSearchParams();
    params.append('status', status);
    params.append('id', testimonialId + '');

    return this._http.post(AppSettings.TESTIMONIAL_CHANGE_STATUS, params, options)
      .map((response) => response.json());
  }
}
