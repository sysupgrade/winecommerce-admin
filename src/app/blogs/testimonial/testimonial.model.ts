export class TestimonialModel {
  static readonly STATUS_ACTIVE: string = '10';
  static readonly STATUS_PENDING: string = '11';
  static readonly STATUS_DELETE: string = '0';

  id: string;
  jobTitle: string;
  personName: string;
  personPictureId: string;
  message: string;
  createdAt: string;
  statusText: string;
  updatedAt: string;
  status: string;
  personNameChinese: string;
  jobTitleChinese: string;
  messageChinese: string;

}


export class TestimonialResponse {

  status: number;
  model: TestimonialModel;
  models: TestimonialModel[] = [];
  total: number;
}
