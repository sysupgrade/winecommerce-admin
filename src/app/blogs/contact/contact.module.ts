import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ContactService } from '../../services/contact/contact.service';
import { DateService } from '../../date.service';
import { RouterModule } from '@angular/router';

export const routes = [
  {
    path: '',
    component: ContactComponent
  }
];

@NgModule({
  imports: [
    CommonModule, NgxDatatableModule, RouterModule.forChild(routes)
  ],
  declarations: [ContactComponent],
  providers: [ContactService, DateService]
})
export class ContactModule { }
