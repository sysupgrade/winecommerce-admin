import { Component, OnInit } from '@angular/core';
import { ContactModel } from '../../models/contact.model';
import { DateService } from '../../date.service';
import { ContactService } from '../../services/contact/contact.service';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'th-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  models : ContactModel[] = [];

  page : number = 1;

  pageSize : number = 10;

  total : number = 0;

  constructor(public dateService : DateService,
      private _authService  :AuthService,
      private _contactService : ContactService) { }

  ngOnInit() {
    this.fetchPage(this.page);
  }

  fetchPage(page : number) {
    this.page = page;
    this._contactService.list(this._authService.getToken(), this.page, this.pageSize)
        .subscribe((response) => {
      if(!response.status) return;
      this.models = response.models;
      this.total = response.total;
    });
  }

}
