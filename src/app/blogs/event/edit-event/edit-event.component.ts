import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EventModel, EventResponse} from '../event.model';
import {Validators} from '@angular/forms';

// import { convertDateTimeLocalToTimestamp, convertTimestampToDateTimeLocal } from '../../../toolkit/library';

@Component({
  selector: 'th-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {

  readonly TITLE: string = 'title';

  readonly TITLE_CHINESE: string = 'titleChinese';

  readonly DESCRIPTION_CHINESE: string = 'descriptionChinese';

  readonly DESCRIPTION: string = 'description';

  readonly ID: string = 'id';

  readonly START_AT: string = 'startAt';

  readonly END_AT: string = 'endAt';

  @Input()
  model: EventModel = new EventModel();

  url: string = './api/blog/event/edit';

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }


  getRules(): Object {
    let json: Object = {};
    json[this.TITLE] = [this.model.title, [Validators.required, Validators.minLength(3)]];
    json[this.ID] = [this.model.id, [Validators.required]];
    json[this.DESCRIPTION] = [this.model.title, [Validators.required]];
    json[this.START_AT] = [this.model.startAt, [Validators.required]];
    json[this.END_AT] = [this.model.endAt, [Validators.required]];
    json[this.TITLE_CHINESE] = [this.model.titleChinese, [Validators.required]];
    json[this.DESCRIPTION_CHINESE] = [this.model.descriptionChinese, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.ID] = {'required': 'Id is required'};
    json[this.TITLE] = {'required': 'Title is required'};
    json[this.DESCRIPTION] = {'required': 'Description is required'};
    json[this.START_AT] = {'required': 'Start time is required'};
    json[this.END_AT] = {'required': 'End time is required'};
    json[this.TITLE_CHINESE] = {'required': 'required'};
    json[this.DESCRIPTION_CHINESE] = {'required': 'required'};
    return json;
  }

  successForm(object: EventResponse) {
    if (!object.status) {
      return;
    }
    this.success.emit(true);
  }

  // convertTimestampToDateTimeLocalAlt(timestamp : number) : Date {
  //   return new Date(timestamp);
  // }
}
