import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';
import { EventModel, EventResponse, EventStatus } from '../event.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../auth.service';
import { DateService } from '../../../date.service';
import { FileService } from '../../../file.service';

@Component({
  selector: 'th-view-event',
  templateUrl: './view-event.component.html',
  styleUrls: ['./view-event.component.scss']
})
export class ViewEventComponent implements OnInit {

  model: EventModel;

  editMode: boolean = false;

  deleteConfirmDialogOpened: boolean = false;

  constructor(private _eventService: EventService,
    private _authService: AuthService,
    private _fileService: FileService,
    private _router: Router,
    private _dateService: DateService,
    private _activatedRoute: ActivatedRoute) {
    this._activatedRoute.params.subscribe((params) => {
      this.fetchData(params['id']);
    });
  }

  ngOnInit() {
  }

  fetchData(eventId: number) {
    this._eventService.getById(eventId, this._authService.getToken())
      .subscribe((response) => {
      if (!response.status) return;
      this.model = response.model;
    });
  }


  getDateTime(timestamp: string): string {
    return this._dateService.getDateTime(timestamp);
  }

  setEditMode(editMode: boolean) {
    this.editMode = editMode;
  }

  generateImagePathById(imageId: string): string {
    return this._fileService.generateImagePathById(imageId);
  }

  handleEditEventSuccess() {
    this.fetchData(this.model.id);
    this.editMode = false;
  }

  publish() {
    this._eventService.changeStatus(this.model.id, this._authService.getToken(), EventStatus.STATUS_ACTIVE)
      .subscribe((response: EventResponse) => this.fetchData(this.model.id));
  }

  deactivate() {
    this._eventService.changeStatus(this.model.id, this._authService.getToken(), EventStatus.STATUS_PENDING)
      .subscribe((response: EventResponse) => this.fetchData(this.model.id));

  }

  delete() {
    this._eventService.changeStatus(this.model.id, this._authService.getToken(), EventStatus.STATUS_DELETE)
      .subscribe((response: EventResponse) => {
        this.fetchData(this.model.id);
        this._router.navigate([this._eventService.getListEventUrl()]);

      });
  }

  showDeleteDialog() {
    this.deleteConfirmDialogOpened = true;
  }

}
