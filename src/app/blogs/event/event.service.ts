import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { EventResponse, EventModel } from './event.model';
import { convertObjectToURLSearchParams } from '../../toolkit/library';

@Injectable()
export class EventService {

  constructor(private _http : Http) { }

  
  add(model : EventModel, token : string) : Observable<EventResponse> {
    let headers : Headers = new Headers();
    headers.append("Authorization", "Bearer " + token);

    let params : URLSearchParams = convertObjectToURLSearchParams(model);
    return this._http.post("./api/blog/event/add", params, {headers : headers})
          .map((response) => response.json());
  }

  list(page: number, pageSize: number, token: string): Observable<EventResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');

    return this._http.get("./api/blog/event/list" + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  getById(eventId: number, token: string): Observable<EventResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    return this._http.get("./api/blog/event/get" + '?id=' + eventId, options)
      .map((response) => response.json());
  }

  changeStatus(eventId: number, token: string, status: string): Observable<EventResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params = new URLSearchParams();
    params.append('status', status);
    params.append('id', eventId + '');

    return this._http.post("./api/blog/event/edit", params, options)
      .map((response) => response.json());
  }

  getEventUrl(eventId : number) : string {
    return "/pages/event/view/" + eventId;
  }

  getListEventUrl() : string {
    return "/pages/event";
  }
}
