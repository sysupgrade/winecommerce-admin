import {Component, OnInit} from '@angular/core';
import {FileModel} from '../../../toolkit/toolkit.model';
import {EventModel} from '../event.model';
import {EventService} from '../event.service';
import {Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../auth.service';
import {convertDateTimeLocalToTimestamp} from '../../../toolkit/library';

@Component({
  selector: 'th-add-event-form',
  templateUrl: './add-event-form.component.html',
  styleUrls: ['./add-event-form.component.scss']
})
export class AddEventFormComponent implements OnInit {

  readonly TITLE: string = 'title';

  readonly DESCRIPTION: string = 'description';

  readonly START_AT: string = 'startAt';

  readonly END_AT: string = 'endAt';

  readonly THUMBNAIL_ID: string = 'thumbnailId';

  readonly TITLE_CHINESE: string = 'titleChinese';

  readonly DESCRIPTION_CHINESE: string = 'descriptionChinese';

  model: EventModel = new EventModel;

  constructor(private _eventService: EventService,
              private _authService: AuthService,
              private _router: Router) {
  }

  ngOnInit() {
  }


  handleOnImageSelect(imageIds: FileModel[]) {
    if (imageIds.length == 0) return;
    this.model.thumbnailId = parseInt(imageIds[0].id);
  }


  getRules(): Object {
    let json: Object = {};
    json[this.TITLE] = [this.model.title,
      [Validators.required, Validators.minLength(3)]];
    json[this.START_AT] = [this.model.startAt, [Validators.required]];
    json[this.END_AT] = [this.model.endAt, [Validators.required]];
    json[this.DESCRIPTION] = [this.model.description, [Validators.required]];
    json[this.THUMBNAIL_ID] = [this.model.thumbnailId, [Validators.required]];
    json[this.TITLE_CHINESE] = [this.model.titleChinese, [Validators.required]];
    json[this.DESCRIPTION_CHINESE] = [this.model.descriptionChinese, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.TITLE] = {'required': 'Title is required'};
    json[this.DESCRIPTION] = {'required': 'Description is required'};
    json[this.START_AT] = {'required': 'Start at is required'};
    json[this.END_AT] = {'required': 'End at is required'};
    json[this.THUMBNAIL_ID] = {'required': 'Thumbnail is required'};
    json[this.TITLE_CHINESE] = {'required': 'required'};
    json[this.DESCRIPTION_CHINESE] = {'required': 'required'};
    return json;
  }

  addEvent(object: any) {
    let model = new EventModel;
    model.descriptionChinese = object[this.DESCRIPTION_CHINESE];
    model.titleChinese= object[this.TITLE_CHINESE];
    model.title = object[this.TITLE];
    model.description = object[this.DESCRIPTION];
    model.startAt = convertDateTimeLocalToTimestamp(object[this.START_AT]);
    model.endAt = convertDateTimeLocalToTimestamp(object[this.END_AT]);
    model.thumbnailId = object[this.THUMBNAIL_ID];
    this._eventService.add(model, this._authService.getToken())
      .subscribe((response) => {
        if (!response.status) return;
        this._router.navigate([this._eventService.getEventUrl(response.model.id)]);
      });
  }

}
