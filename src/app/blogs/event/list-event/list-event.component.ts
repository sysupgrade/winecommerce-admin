import { Component, OnInit, Input } from '@angular/core';
import { EventService } from '../event.service';
import { EventModel } from '../event.model';
import { AuthService } from '../../../auth.service';
import { Router } from '@angular/router';
import { DateService } from '../../../date.service';

@Component({
  selector: 'th-list-event',
  templateUrl: './list-event.component.html',
  styleUrls: ['./list-event.component.scss']
})
export class ListEventComponent implements OnInit {

  @Input() page: number = 1;

  @Input() pageSize: number = 10;

  models: EventModel[] = [];

  total: number = 0;

  constructor(private _eventService: EventService,
    private _router: Router,
    private _dateService  : DateService,
    private _authService: AuthService) { }

  ngOnInit() {
    this.fetchPage(this.page);
  }

  fetchPage(page: number) {
    this.page = page;
    this._eventService.list(this.page, this.pageSize, this._authService.getToken())
      .subscribe((response) => {
        if (!response.status) return;
        this.models = response.models;
        this.total = response.total;
      });
  }

  redirectToEventUrl(eventId: number) {
    this._router.navigate([this._eventService.getEventUrl(eventId)]);
  }
  
  getDateTime(timestamp: string): string {
    return this._dateService.getDateTime(timestamp);
  }

}
