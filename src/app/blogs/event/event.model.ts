export class EventModel {
    id : number;
    title : string;
    description : string;
    startAt : number;
    endAt  :number;
    thumbnailId : number;
    status : number;
    createdAt  : number;
    statusText : string;
    updatedAt : number;
    titleChinese : string;
    descriptionChinese : string;
}

export class EventResponse {
    status : number;
    model : EventModel;
    models : EventModel[];
    total : number;
}


export class EventStatus {
    static readonly STATUS_ACTIVE: string = '10';
    static readonly STATUS_PENDING: string = '11';
    static readonly STATUS_DELETE: string = '0';
  }
