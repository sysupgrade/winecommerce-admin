import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEventFormComponent } from './add-event-form/add-event-form.component';
import { ViewEventComponent } from './view-event/view-event.component';
import { ListEventComponent } from './list-event/list-event.component';
import { RouterModule } from '@angular/router';
import { ToolkitModule } from '../../toolkit/toolkit.module';
import { EventService } from './event.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { DateService } from '../../date.service';
import { FileService } from '../../file.service';
import { EditEventComponent } from './edit-event/edit-event.component';

export const routes = [
  {path: '', component: ListEventComponent},
  {path: 'add', component: AddEventFormComponent},
  {path: 'view/:id', component: ViewEventComponent },
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), ToolkitModule,
    NgxDatatableModule, DirectivesModule, PipesModule
  ],
  declarations: [AddEventFormComponent, ViewEventComponent, ListEventComponent, EditEventComponent],
  providers: [EventService, DateService, FileService]
})
export class EventModule { }
