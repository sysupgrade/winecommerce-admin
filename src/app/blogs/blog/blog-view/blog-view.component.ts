import {Component, OnInit} from '@angular/core';
import {BlogModel, BlogResponse, BlogStatus} from '../blog.model';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {BlogService} from '../blog.service';
import {DateService} from '../../../date.service';
import {FileService} from '../../../file.service';
import {AuthService} from '../../../auth.service';

@Component({
  selector: 'th-blog-view',
  templateUrl: './blog-view.component.html',
  styleUrls: ['./blog-view.component.scss'],
  providers : [BlogService, DateService, FileService, DateService]
})
export class BlogViewComponent implements OnInit {

  blog: BlogModel = new BlogModel();

  isEditMode: boolean = false;

  blogId: number = null;

  isDeleteConfirmDialogOpened: boolean = false;

  constructor(private _blogService: BlogService,
              private _dateService: DateService,
              private _fileService: FileService,
              private _authService: AuthService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe((params: Params) => {
      this.blogId = params['id'];
      this.getData();
    });
  }

  getData() {
    this._blogService.getById(this.blogId, this._authService.getToken())
      .subscribe((response: BlogResponse) => {
        if (!response.status) return;
        this.blog = response.model;
      });
  }

  getDateTime(timestamp: string): string {
    return this._dateService.getDateTime(timestamp);
  }

  setEditMode(editMode: boolean) {
    this.isEditMode = editMode;
  }

  generateImagePathById(imageId: string): string {
    return this._fileService.generateImagePathById(imageId);
  }

  handleEditBlogSuccess() {
    this.getData();
    this.isEditMode = false;
  }

  publish() {
    this._blogService.changeStatus(this.blogId, this._authService.getToken(), BlogStatus.STATUS_ACTIVE)
      .subscribe((response: BlogResponse) => this.getData());
  }

  deactivate() {
    this._blogService.changeStatus(this.blogId, this._authService.getToken(), BlogStatus.STATUS_PENDING)
      .subscribe((response: BlogResponse) => this.getData());

  }

  delete() {
    this._blogService.changeStatus(this.blogId, this._authService.getToken(), BlogStatus.STATUS_DELETE)
      .subscribe((response: BlogResponse) => {
        this.getData();
        // this._router.navigate([this._blogService.getListBlogUrl()]);

      });
  }

  showDeleteDialog() {
    this.isDeleteConfirmDialogOpened = true;
  }
}
