import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BlogAddFormComponent} from './blog-add-form/blog-add-form.component';
import {BlogViewComponent} from './blog-view/blog-view.component';
import {BlogEditFormComponent} from './blog-edit-form/blog-edit-form.component';
import {BlogListComponent} from './blog-list/blog-list.component';
import {BlogFilterFormComponent} from './blog-filter-form/blog-filter-form.component';
import {RouterModule} from '@angular/router';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {DirectivesModule} from '../../theme/directives/directives.module';
import {PipesModule} from '../../theme/pipes/pipes.module';

export const routes = [
  {path: '', component: BlogListComponent},
  {path: 'add', component: BlogAddFormComponent},
  {path: 'edit', component: BlogEditFormComponent},
  {path: ':id', component: BlogViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    DirectivesModule,
    PipesModule,
    RouterModule.forChild(routes),
  ],

  declarations: [BlogAddFormComponent, BlogEditFormComponent, BlogListComponent, BlogViewComponent, BlogFilterFormComponent],
  exports: [BlogAddFormComponent, BlogEditFormComponent, BlogListComponent, BlogViewComponent, BlogFilterFormComponent],
})
export class BlogModule {
}
