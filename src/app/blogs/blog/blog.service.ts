import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {BlogResponse} from './blog.model';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {AppSettings} from '../../app.setting';

@Injectable()
export class BlogService {

  constructor(private _http: Http) {
  }

  list(page: number, pageSize: number, token: string): Observable<BlogResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');

    return this._http.get("./api/blog/blog/list" + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  getById(blogId: number, token: string): Observable<BlogResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    return this._http.get("./api/blog/blog/get" + '?id=' + blogId, options)
      .map((response) => response.json());
  }

  changeStatus(blogId: number, token: string, status: string): Observable<BlogResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params = new URLSearchParams();
    params.append('status', status);
    params.append('id', blogId + '');

    return this._http.post("./api/blog/blog/edit", params, options)
      .map((response) => response.json());
  }
}
