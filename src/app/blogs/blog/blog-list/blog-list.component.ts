import {Component, Input, OnInit} from '@angular/core';
import {BlogService} from '../blog.service';
import {DateService} from '../../../date.service';
import {BLOG_URL, BlogModel, BlogResponse} from '../blog.model';
import {Router} from '@angular/router';
import {AuthService} from '../../../auth.service';

@Component({
  selector: 'th-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss'],
  providers: [BlogService, DateService]
})
export class BlogListComponent implements OnInit {

  @Input() page: number = 1;

  @Input() pageSize: number = 10;

  data: BlogModel[] = [];

  total: number = 0;

  constructor(private _blogService: BlogService,
              private _dateService: DateService,
              private _router: Router,
              private _authService: AuthService) {
  }

  ngOnInit() {
    this.getPage(this.page);
  }

  getPage(page: number) {
    this.page = page;
    this._blogService.list(this.page, this.pageSize, this._authService.getToken())
      .subscribe(this.handleListResponse.bind(this));
  }

  handleListResponse(response: BlogResponse) {
    this.data = response.models;
    this.total = response.total;
  }

  getDateTime(timestamp: string): string {
    return this._dateService.getDateTime(timestamp);
  }

  redirectToBlogUrl(blog: BlogModel) {
    // this._router.navigate([this._blogService.getViewBlogUrl(blog.id)]);
    this._router.navigate([BLOG_URL.ROOT, blog.id]);
  }
}
