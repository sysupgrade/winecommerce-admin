export class BlogModel {
  id: string;
  title: string;
  creatorId: string;
  blog: string;
  createdAt: string;
  updatedAt: string;
  status: string;
  thumbnailId: string;
  statusText: string;
  blogChinese: string;
  titleChinese: string;
}

export class BlogResponse {
  model: BlogModel;
  models: BlogModel[] = [];
  total: number;
  status: string;
}


export class BLOG_URL {
  public static ROOT = '/pages/news/';
}

export class FilterBlogModel {
  userId: string;
  status: string;
  search: string;
}


export class BlogStatus {
  static readonly STATUS_ACTIVE: string = '10';
  static readonly STATUS_PENDING: string = '11';
  static readonly STATUS_DELETE: string = '0';
}
