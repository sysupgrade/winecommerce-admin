import {Component, OnInit} from '@angular/core';
import {BlogService} from '../blog.service';
import {BLOG_URL, BlogModel, BlogResponse} from '../blog.model';
import {Router} from '@angular/router';
import {Validators} from '@angular/forms';
import {FileModel} from '../../../toolkit/toolkit.model';

@Component({
  selector: 'th-blog-add-form',
  templateUrl: './blog-add-form.component.html',
  styleUrls: ['./blog-add-form.component.scss'],
  providers: [BlogService]
})
export class BlogAddFormComponent implements OnInit {

  readonly TITLE: string = 'title';

  readonly BLOG: string = 'blog';

  readonly BLOG_CHINESE: string = 'blogChinese';

  readonly TITLE_CHINESE: string = 'titleChinese';

  readonly THUMBNAIL_ID: string = 'thumbnailId';

  model: BlogModel = new BlogModel();

  url: string = './api/blog/blog/add';

  constructor(private _router: Router) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.TITLE] = [this.model.title, [Validators.required, Validators.minLength(3)]];
    json[this.BLOG] = [this.model.title, [Validators.required]];
    json[this.THUMBNAIL_ID] = [this.model.thumbnailId, [Validators.required]];
    json[this.TITLE_CHINESE] = [this.model.id, [Validators.required]];
    json[this.BLOG_CHINESE] = [this.model.id, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.TITLE] = {'required': 'Title is required'};
    json[this.BLOG_CHINESE] = {'required': 'required'};
    json[this.TITLE_CHINESE] = {'required': 'required'};
    json[this.BLOG] = {'required': 'Blog is required'};
    json[this.THUMBNAIL_ID] = {'required': 'Thumbnail is required'};
    return json;
  }

  successForm(object: BlogResponse) {
    if (!object.status) {
      return;
    }
    this._router.navigate([BLOG_URL.ROOT, object.model.id]);
    this.model = new BlogModel();
  }

  handleOnImageSelect(imageIds: FileModel[]) {
    if (imageIds.length == 0) return;
    this.model.thumbnailId = imageIds[0].id;
  }
}
