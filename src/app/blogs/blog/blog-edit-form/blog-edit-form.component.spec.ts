import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BlogEditFormComponent} from './blog-edit-form.component';

describe('BlogEditFormComponent', () => {
  let component: BlogEditFormComponent;
  let fixture: ComponentFixture<BlogEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlogEditFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
