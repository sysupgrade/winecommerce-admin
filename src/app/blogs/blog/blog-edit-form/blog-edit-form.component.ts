import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppSettings} from '../../../app.setting';
import {BlogModel, BlogResponse} from '../blog.model';
import {Validators} from '@angular/forms';

@Component({
  selector: 'th-blog-edit-form',
  templateUrl: './blog-edit-form.component.html',
  styleUrls: ['./blog-edit-form.component.scss']
})
export class BlogEditFormComponent implements OnInit {

  readonly TITLE: string = 'title';

  readonly BLOG: string = 'blog';

  readonly ID: string = 'id';

  readonly BLOG_CHINESE: string = 'blogChinese';

  readonly TITLE_CHINESE: string = 'titleChinese';

  @Input()
  model: BlogModel = new BlogModel();

  url: string = AppSettings.BLOG_EDIT;

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }


  getRules(): Object {
    let json: Object = {};
    json[this.TITLE] = [this.model.title, [Validators.required, Validators.minLength(3)]];
    json[this.ID] = [this.model.id, [Validators.required]];
    json[this.TITLE_CHINESE] = [this.model.id, [Validators.required]];
    json[this.BLOG_CHINESE] = [this.model.id, [Validators.required]];
    json[this.BLOG] = [this.model.title, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.ID] = {'required': 'Id is required'};
    json[this.TITLE] = {'required': 'Title is required'};
    json[this.BLOG] = {'required': 'Blog is required'};
    json[this.BLOG_CHINESE] = {'required': 'required'};
    json[this.TITLE_CHINESE] = {'required': 'required'};
    return json;
  }

  successForm(object: BlogResponse) {
    if (!object.status) {
      return;
    }
    this.success.emit(true);
  }
}
