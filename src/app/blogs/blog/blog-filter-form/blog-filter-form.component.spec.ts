import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BlogFilterFormComponent} from './blog-filter-form.component';

describe('BlogFilterFormComponent', () => {
  let component: BlogFilterFormComponent;
  let fixture: ComponentFixture<BlogFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlogFilterFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
