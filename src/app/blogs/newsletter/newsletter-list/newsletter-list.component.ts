import {Component, Input, OnInit} from '@angular/core';
import {DateService} from '../../../date.service';
import {NewsletterModel, NewsletterResponse} from '../newsletter.model';
import {AuthService} from '../../../auth.service';
import {Router} from '@angular/router';
import {NewsletterService} from '../newsletter.service';

@Component({
  selector: 'th-newsletter-list',
  templateUrl: './newsletter-list.component.html',
  styleUrls: ['./newsletter-list.component.scss'],
  providers: [NewsletterService, DateService]
})
export class NewsletterListComponent implements OnInit {

  @Input() page: number = 1;

  @Input() pageSize: number = 10;

  models: NewsletterModel[] = [];

  total: number = 0;

  constructor(private _newsletterService: NewsletterService,
              private _dateService: DateService,
              private _router: Router,
              private _authService: AuthService) {
  }

  ngOnInit() {
    this.getPage(this.page);
  }

  getPage(page: number) {
    this.page = page;
    this._newsletterService.list(this.page, this.pageSize,
      this._authService.getToken())
      .subscribe((response: NewsletterResponse) => {
        this.models = response.models;
        this.total = response.total;
      });
  }

  getDateTime(createdAt: string): string {
    return this._dateService.getDateTime(createdAt);
  }

  redirectToNewsletterUrl(newsletterId: string) {
    this._router.navigate([this._newsletterService.getViewUrl(newsletterId)]);
  }
}
