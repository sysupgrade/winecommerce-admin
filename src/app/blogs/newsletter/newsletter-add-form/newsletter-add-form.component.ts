import {Component, OnInit} from '@angular/core';
import {NewsletterService} from '../newsletter.service';
import {NewsletterModel, NewsletterResponse} from '../newsletter.model';
import {AppSettings} from '../../../app.setting';
import {Router} from '@angular/router';
import {Validators} from '@angular/forms';

@Component({
  selector: 'th-newsletter-add-form',
  templateUrl: './newsletter-add-form.component.html',
  styleUrls: ['./newsletter-add-form.component.scss'],
  providers: [NewsletterService]
})
export class NewsletterAddFormComponent implements OnInit {

  url: string = AppSettings.NEWS_LETTER_ADD;

  readonly TITLE: string = 'title';

  readonly NEWSLETTER: string = 'newsletter';

  model: NewsletterModel = new NewsletterModel();

  constructor(private _router: Router,
              private _newsletterService: NewsletterService) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.TITLE] = [this.model.title,
      [Validators.required, Validators.minLength(3)]];
    json[this.NEWSLETTER] = [this.model.newsletter, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.TITLE] = {'required': 'Title is required'};
    json[this.NEWSLETTER] = {'required': 'Newsletter is required'};
    return json;
  }

  successForm(object: NewsletterResponse) {
    if (!object.status) {
      return;
    }
    this._router.navigate([this._newsletterService.getViewUrl(object.model.id)]);
    this.model = new NewsletterModel;
  }

}
