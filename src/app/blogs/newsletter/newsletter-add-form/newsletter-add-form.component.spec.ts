import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewsletterAddFormComponent} from './newsletter-add-form.component';

describe('NewsletterAddFormComponent', () => {
  let component: NewsletterAddFormComponent;
  let fixture: ComponentFixture<NewsletterAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewsletterAddFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsletterAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
