import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {NewsletterResponse} from './newsletter.model';
import {AppSettings} from '../../app.setting';

@Injectable()
export class NewsletterService {

  constructor(private _http: Http) {
  }

  list(page: number, pageSize: number, token: string): Observable<NewsletterResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');

    return this._http.get(AppSettings.NEWS_LETTER_LIST + '?' + params.toString(), options)
      .map((response) => response.json());

  }

  getViewUrl(newsletterId: string): string {
    return 'pages/newsletter/view/' + newsletterId;
  }
}
