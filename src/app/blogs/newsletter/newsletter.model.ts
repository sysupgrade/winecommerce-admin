export class NewsletterModel {
  id: string;
  newsletter: string;
  title: string;
  createdAt: number;
  updatedAt: number;
}

export class NewsletterResponse {
  status: number;
  model: NewsletterModel;
  models: NewsletterModel[] = [];
  total: number;
}
