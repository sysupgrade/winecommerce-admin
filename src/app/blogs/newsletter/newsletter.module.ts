import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewsletterAddFormComponent} from './newsletter-add-form/newsletter-add-form.component';
import {NewsletterListComponent} from './newsletter-list/newsletter-list.component';
import {NewsletterViewComponent} from './newsletter-view/newsletter-view.component';
import {PipesModule} from '../../theme/pipes/pipes.module';
import {RouterModule} from '@angular/router';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {DirectivesModule} from '../../theme/directives/directives.module';

export const routes = [
  {path: '', component: NewsletterListComponent},
  {path: 'add', component: NewsletterAddFormComponent},
  {path: 'view/:id', component: NewsletterViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    DirectivesModule,
    PipesModule,
    RouterModule.forChild(routes),
  ],

  declarations: [NewsletterAddFormComponent, NewsletterListComponent, NewsletterViewComponent],
  exports: [NewsletterAddFormComponent, NewsletterListComponent, NewsletterViewComponent],
})
export class NewsletterModule {
}
