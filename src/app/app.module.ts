import {HttpModule} from '@angular/http';
import {AuthService} from './auth.service';
import {AppGuard} from './app.guard';
import 'pace';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AgmCoreModule} from '@agm/core';

import {routing} from './app.routing';
import {AppConfig} from './app.config';

import {AppComponent} from './app.component';
import {ErrorComponent} from './pages/error/error.component';


@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDe_oVpi9eRSN99G4o6TwVjJbFBNr58NxE'
    }),
    routing
  ],
  providers: [AppConfig, AppGuard, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
