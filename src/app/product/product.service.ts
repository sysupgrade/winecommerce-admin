import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ProductResponse} from './product.model';
import {AppSettings} from '../app.setting';

@Injectable()
export class ProductService {

  constructor(private _http: Http) {
  }

  list(page: number, pageSize: number, status: string, search: string, startDate: string, endDate: string): Observable<ProductResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('page', page + '');
    params.append('pageSize', pageSize + '');
    if (status) params.append('status', status + "");
    if (search) params.append('q', search);
    if (startDate) params.append('startDate', startDate);
    if (endDate) params.append('endDate', endDate);

    return this._http.get(AppSettings.PRODUCT_LIST + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  get(productId: string): Observable<ProductResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', productId);

    return this._http.get(AppSettings.PRODUCT_GET + '?' + params.toString(), options)
      .map((response) => response.json());
  }

  addProductCategory(productId: string, categoryId: string, token: string): Observable<ProductResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('productId', productId);
    params.append('categoryId', categoryId);

    return this._http.post(AppSettings.PRODUCT_ADD_CATEGORY, params, options)
      .map((response) => response.json());
  }

  addProductAttribute(productId: string, attributeId: string, attributeValue: string, token: string): Observable<ProductResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('productId', productId);
    params.append('attributeId', attributeId);
    params.append('attributeValue', attributeValue);

    return this._http.post(AppSettings.PRODUCT_ADD_ATTRIBUTE, params, options)
      .map((response) => response.json());
  }

  changeStatus(productId: string, status: string, token: string): Observable<ProductResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', productId);
    params.append('status', status);

    return this._http.post(AppSettings.PRODUCT_CHANGE_STATUS, params, options)
      .map((response) => response.json());
  }


  addProductImage(productId: string, imageId: string,token: string): Observable<ProductResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('productId', productId);
    params.append('imageId', imageId);

    return this._http.post(AppSettings.PRODUCT_ADD_IMAGE, params, options)
      .map((response) => response.json());
  }

  productImageStatus(productId: string, imageId: string, status : string, token: string, newImageId: string): Observable<any> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('productId', productId);
    params.append('imageId', imageId);
    if(status) params.append('status', status);
    if(newImageId) params.append('newImageId', newImageId);

    return this._http.post(AppSettings.PRODUCT_IMAGE_STATUS, params, options)
      .map((response) => response.json());
  }

  removeProductAttribute(productId: string, attributeId: string, token: string): Observable<ProductResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('productId', productId);
    params.append('attributeId', attributeId);

    return this._http.post(AppSettings.PRODUCT_REMOVE_ATTRIBUTE, params, options)
      .map((response) => response.json());
  }

  removeProductCategory(productId: string, categoryId: string, token: string): Observable<ProductResponse> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});

    let params: URLSearchParams = new URLSearchParams();
    params.append('productId', productId);
    params.append('categoryId', categoryId);

    return this._http.post(AppSettings.PRODUCT_REMOVE_CATEGORY, params, options)
      .map((response) => response.json());
  }
}
