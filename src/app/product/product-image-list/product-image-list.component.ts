import {Component, Input, OnInit} from '@angular/core';
import {ProductService} from '../product.service';
import {FileService} from '../../file.service';
import {FileModel} from '../../toolkit/toolkit.model';
import {AuthService} from '../../auth.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'th-product-image-list',
  templateUrl: './product-image-list.component.html',
  styleUrls: ['./product-image-list.component.scss'],
  providers: [ProductService, FileService, AuthService]
})
export class ProductImageListComponent implements OnInit {


  @Input()
  productId: string;

  @Input()
  models: string[] = [];

  upload: boolean[] = [];

  imageId: string;

  newImageId: string = '0';

  newChaneImage: string;

  addImage: boolean = false;

  total: number;

  isDeleteConfirmDialogOpened: boolean = false;

  selectedDeleteImage: string;

  constructor(private _productService: ProductService,
              private _activatedRoute: ActivatedRoute,
              private _authService: AuthService,
              private _fileService: FileService) {
  }

  ngOnInit() {
  }

  getFilePath(imageId: string) {
    return this._fileService.generateImagePathById(imageId);
  }

  previewImage(model: FileModel[], imageId: string, idx: string) {
    this.imageId = imageId;
    this.models[idx] = model[0].id;
    this.upload[idx] = true;
    this.newChaneImage = model[0].id;
  }


  changeImage(event: any, idx: number) {
    this._productService.productImageStatus(this.productId, this.imageId, null, this._authService.getToken(), this.newChaneImage)
      .subscribe(response => {
      });

    console.log(idx);
    this.upload[idx] = false;
  }

  cancelChangeImage(event: Event, imageId: number) {
    this.upload[imageId] = false;
  }


  previewImageAdd(model: FileModel[]) {
    this.newImageId = model[0].id;
    this.addImage = true;
  }

  addNewImage() {
    this._productService.addProductImage(this.productId, this.newImageId, this._authService.getToken())
      .subscribe(response => {
        this.models.push(this.newImageId);
        this.newImageId = '0';
        this.addImage = false;
      });


  }

  canceladdNewImage() {
    this.addImage = false;
  }

  showDeleteDialog(i: string) {
    this.selectedDeleteImage = i;
    this.isDeleteConfirmDialogOpened = true;
  }

  delete() {
    this._productService.productImageStatus(this.productId, this.selectedDeleteImage, '0', this._authService.getToken(), null)
      .subscribe(response => {
        this.models = response.models;
      });
  }

  getImageDelete() {
    return this._fileService.generateImagePathById(this.selectedDeleteImage);
  }
}
