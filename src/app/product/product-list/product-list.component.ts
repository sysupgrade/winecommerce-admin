import {Component, Input, OnInit} from '@angular/core';
import {ProductService} from '../product.service';
import {FilterProductModel, PRODUCT_URL, ProductModel} from '../product.model';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';
import {ToasterService} from '../../toaster.service';
import {DateService} from '../../date.service';

@Component({
  selector: 'th-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  providers: [ProductService, AuthService, ToasterService, DateService]
})
export class ProductListComponent implements OnInit {

  @Input()
  page: number = 1;

  @Input()
  pageSize: number = 10;

  status: string = '10';

  search: string = null;

  startDate: string = null;

  endDate: string = null;

  models: ProductModel[] = [];

  total: number = 0;

  isDialogConfirm: boolean = false;

  model: ProductModel = new ProductModel();

  statusSelected: string;

  constructor(
    private _productService: ProductService,
    private _router: Router,
    private _authService: AuthService,
    private _toasterService: ToasterService,
    private _dateService: DateService
  ) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._productService.list(this.page, this.pageSize, this.status, this.search, this.startDate, this.endDate)
      .subscribe((response) => {
        this.models = response.models;
        this.total = response.total;
      });

  }

  getProductURL(id: string) {
    this._router.navigate([PRODUCT_URL.ROOT, id])
  }

  handleOnFilter(model: FilterProductModel) {
    this.search = model.q;
    this.status = model.status;
    this.startDate = model.startDate;
    this.endDate = model.endDate;
    this.list(this.page);
  }

  openDialogConfirm(val: boolean, data: ProductModel) {
    this.model = data;
    this.isDialogConfirm = val;
  }

  changeStatus(productId: string, status: string) {
    if (status == '10') this.statusSelected = 'APPROVED';
    if (status == '11') this.statusSelected = 'PENDING';
    if (status == '0') this.statusSelected = 'DELETE';

    this._productService.changeStatus(productId, status, this._authService.getToken())
      .subscribe((response) => {
        if (response.status == '1')
          this.list(this.page);
        this._toasterService.openToast(this.statusSelected, '', 'success');
      });
  }

  getDate(date: string): string {
    return this._dateService.getDateNumberFormat(date);
  }

}
