import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductModel, ProductResponse} from '../product.model';
import {Validators} from '@angular/forms';
import {AppSettings} from '../../app.setting';
import {ProductService} from '../product.service';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'th-product-edit-form',
  templateUrl: './product-edit-form.component.html',
  styleUrls: ['./product-edit-form.component.scss']
})
export class ProductEditFormComponent implements OnInit {

  ckEditorConfig = {
    removePlugins: 'save, newpage, print'
  };

  readonly ID: string = 'id';

  readonly NAME: string = 'name';

  readonly DEFAULT_PRICE: string = 'defaultPrice';

  readonly DESCRIPTION: string = 'description';

  readonly QUANTITY: string = 'quantity';

  readonly ALCOHOL_PERCENTAGE: string = 'alcoholPercentage';

  readonly VOLUME_IN_ML: string = 'volumeInMl';

  readonly POINT_SYSTEM: string = 'pointSystem';

  readonly PRICE_BEFORE_DISCOUNT: string = 'priceBeforeDiscount';

  readonly NAME_CHINESE: string = 'nameChinese';

  readonly DESCRIPTION_CHINESE: string = 'descriptionChinese';


  url: string = AppSettings.PRODUCT_EDIT;

  @Input()
  model: ProductModel = new ProductModel();

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private _productService: ProductService,
              private _authService: AuthService) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.model.name, [Validators.required, Validators.minLength(3)]];
    json[this.NAME_CHINESE] = [this.model.nameChinese, [Validators.required]];
    json[this.DEFAULT_PRICE] = [this.model.defaultPrice, [Validators.required]];
    json[this.DESCRIPTION] = [this.model.description];
    json[this.DESCRIPTION_CHINESE] = [this.model.descriptionChinese, [Validators.required]];
    json[this.ALCOHOL_PERCENTAGE] = [null, [Validators.required]];
    json[this.VOLUME_IN_ML] = [null, [Validators.required]];
    json[this.ID] = [this.model.id];
    json[this.QUANTITY] = [this.model.quantity];
    json[this.POINT_SYSTEM] = [this.model.pointSystem];
    json[this.PRICE_BEFORE_DISCOUNT] = [this.model.priceBeforeDiscount];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'product name is required'};
    json[this.NAME_CHINESE] = {'required': 'product name is required'};
    json[this.DEFAULT_PRICE] = {'required': 'price is required'};
    json[this.DESCRIPTION] = {};
    json[this.DESCRIPTION_CHINESE] = {};
    json[this.ID] = {'required': 'id is required'};
    json[this.QUANTITY] = {};
    json[this.POINT_SYSTEM] = {'required': 'system point is required'};
    json[this.PRICE_BEFORE_DISCOUNT] = {};
    return json;
  }

  successForm(object: ProductResponse) {
    if (!object) return;
    this.success.emit(true);
    // this.saveAttribute(this.model.id);

  }


  handleAddAttributeProduct(event) {
    this._productService.addProductAttribute(this.model.id, event.attributeId, event.attributeValue, this._authService.getToken()).subscribe(response => {
      if (response.status) {
        this.model.attributes.push(event);
      }
    });

  }

  handleRemoveAttribute(event) {
    console.log(event);
    this._productService.removeProductAttribute(this.model.id, event.id, this._authService.getToken()).subscribe(response => {
      if (response.status) {
        this.model.attributes.splice(this.model.attributes.indexOf(event), 1);
      }
    });

  }


  handleAddCategoryProduct(event) {
    this._productService.addProductCategory(this.model.id, event.id, this._authService.getToken()).subscribe(response => {
      if (response.status) {
        this.model.categories.push(event);
      }
    });

  }

  handleRemoveCategory(event) {
    this._productService.removeProductCategory(this.model.id, event.id, this._authService.getToken()).subscribe(response => {
      if (response.status) {
        this.model.categories.splice(this.model.categories.indexOf(event), 1);
      }
    });
  }

}
