import {Component, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../product.service';
import {ProductModel} from '../product.model';
import {FileService} from '../../file.service';

@Component({
  selector: 'th-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss'],
  providers: [ProductService, FileService]
})
export class ProductViewComponent implements OnInit {

  productId: string;

  @Output()
  model: ProductModel;

  isEditMode: boolean = false;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _productService: ProductService,
    private _fileService : FileService
  ) {
  }

  ngOnInit() {
    this.model = new ProductModel();
    this._activatedRoute.params.subscribe(params => {
      this.productId = params['id'];
      this.getData();
    });
  }

  getData() {
    this._productService.get(this.productId)
      .subscribe((response) => {
        this.model = response.model;
      });
  }

  setEditModel(val: boolean) {
    this.isEditMode = val;
  }


}
