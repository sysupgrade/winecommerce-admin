import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductViewComponent} from './product-view/product-view.component';
import {ProductEditFormComponent} from './product-edit-form/product-edit-form.component';
import {ProductAddFormComponent} from './product-add-form/product-add-form.component';
import {RouterModule} from '@angular/router';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ToolkitModule} from '../toolkit/toolkit.module';
import {DirectivesModule} from '../theme/directives/directives.module';
import {ProductAttributeFormComponent} from './product-attribute-form/product-attribute-form.component';
import {ProductCategoryFormComponent} from './product-category-form/product-category-form.component';
import {ProductFilterFormComponent} from './product-filter-form/product-filter-form.component';
import {ProductStockListComponent} from './product-stock-list/product-stock-list.component';
import {ProductStockFormComponent} from './product-stock-form/product-stock-form.component';
import {PipesModule} from '../theme/pipes/pipes.module';
import {ProductImageListComponent} from './product-image-list/product-image-list.component';
import {ProductImageFormComponent} from './product-image-form/product-image-form.component';
import {SafeHtmlPipe} from '../SafeHtmlPipe';

export const routes = [
  {path: '', component: ProductListComponent},
  {path: 'add', component: ProductAddFormComponent},
  {path: 'edit', component: ProductEditFormComponent},
  {path: ':id', component: ProductViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ToolkitModule,
    PipesModule,
    DirectivesModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ProductListComponent,
    ProductAddFormComponent,
    ProductEditFormComponent,
    ProductViewComponent,
    ProductAttributeFormComponent,
    ProductCategoryFormComponent,
    ProductFilterFormComponent,
    ProductStockListComponent,
    ProductStockFormComponent,
    ProductImageListComponent,
    ProductImageFormComponent,
    SafeHtmlPipe
  ],

  exports: [ProductListComponent,
    ProductAddFormComponent,
    ProductEditFormComponent,
    ProductViewComponent,
    ProductAttributeFormComponent,
    ProductCategoryFormComponent,
    ProductFilterFormComponent,
    ProductStockListComponent,
    ProductStockFormComponent,
    ProductImageListComponent,
    ProductImageFormComponent,
  ],
})
export class ProductModule {
}
