import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DropdownItemModel} from '../../toolkit/toolkit.model';
import {FilterProductModel} from '../product.model';

@Component({
  selector: 'th-product-filter-form',
  templateUrl: './product-filter-form.component.html',
  styleUrls: ['./product-filter-form.component.scss']
})
export class ProductFilterFormComponent implements OnInit {

  statusValue: string = null;

  q: string = null;

  statusText: string = null;

  @Input()
  id: string;

  total: number;

  statusItems: DropdownItemModel[] = [
    {
      text: 'Accept',
      value: '10'
    }, {
      text: 'Pending',
      value: '11'
    }, {
      text: 'Delete',
      value: '0'
    }];

  @Output()
  filter: EventEmitter<FilterProductModel> = new EventEmitter<FilterProductModel>();

  constructor() {
  }

  ngOnInit() {
  }

  handleOnFilter(event, startDate: string, endDate: string) {
    let dateStart = Date.parse(startDate).toString();
    let intDateStart = dateStart.substring(0, dateStart.length - 3);
    let dateEnd = Date.parse(endDate).toString();
    let intDateEnd = dateEnd.substring(0, dateEnd.length - 3);

    this.filter.emit({
      q: this.q,
      status: this.statusValue,
      startDate: intDateStart,
      endDate: intDateEnd
    });

  }

  resetFilter() {
    this.q = null;
    this.statusValue = null;
    this.statusText = null;
    this.handleOnFilter(null, null, null);
  }

}
