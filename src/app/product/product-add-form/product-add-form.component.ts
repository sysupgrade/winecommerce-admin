import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppSettings} from '../../app.setting';
import {PRODUCT_URL, ProductModel, ProductResponse} from '../product.model';
import {Validators} from '@angular/forms';
import {AttributeProductModel} from '../../attribute/attribute.model';
import {CategoryModel} from '../../category/category.model';
import {ProductService} from '../product.service';
import {AuthService} from '../../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'th-product-add-form',
  templateUrl: './product-add-form.component.html',
  styleUrls: ['./product-add-form.component.scss'],
  providers: [ProductService, AuthService]
})
export class ProductAddFormComponent implements OnInit {

  ckEditorConfig = {
    removePlugins: 'save, newpage, print'
  };

  readonly NAME: string = 'name';

  readonly DEFAULT_PRICE: string = 'defaultPrice';

  readonly DESCRIPTION: string = 'description';

  readonly QUANTITY: string = 'quantity';

  readonly ALCOHOL_PERCENTAGE :  string = "alcoholPercentage";

  readonly VOLUME_IN_ML : string = "volumeInMl";

  readonly POINT_SYSTEM : string = "pointSystem";

  readonly PRICE_BEFORE_DISCOUNT : string = "priceBeforeDiscount";

  readonly NAME_CHINESE : string = "nameChinese";

  readonly DESCRIPTION_CHINESE : string = "descriptionChinese";

  url: string = AppSettings.PRODUCT_ADD;

  @Input()
  model: ProductModel = new ProductModel();

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  attributeProductModels: AttributeProductModel[] = [];

  categoryProductModels: CategoryModel[] = [];


  constructor(private _productService: ProductService,
              private _authService: AuthService,
              private _routerService : Router) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.model.name, [Validators.required, Validators.minLength(3)]];
    json[this.NAME_CHINESE] = [this.model.nameChinese, [Validators.required]];
    json[this.DEFAULT_PRICE] = [this.model.defaultPrice, [Validators.required]];
    json[this.DESCRIPTION] = [this.model.description, [Validators.required]];
    json[this.DESCRIPTION_CHINESE] = [this.model.descriptionChinese, [Validators.required]];
    json[this.QUANTITY] = [this.model.quantity, [Validators.required]];
    json[this.ALCOHOL_PERCENTAGE] = [null, [Validators.required]];
    json[this.VOLUME_IN_ML] = [null, [Validators.required]];
    json[this.POINT_SYSTEM] = [this.model.pointSystem];
    json[this.PRICE_BEFORE_DISCOUNT] = [this.model.priceBeforeDiscount];
    return json;

  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'Product Name is required'};
    json[this.NAME_CHINESE] = {'required': 'Product Name is required'};
    json[this.VOLUME_IN_ML] = {"required": "Volume is required"};
    json[this.ALCOHOL_PERCENTAGE] = {"required": "Alcohol % is required"};
    json[this.DEFAULT_PRICE] = {'required': 'Price is required'};
    json[this.DESCRIPTION] = {"required": "Description is required"};
    json[this.DESCRIPTION_CHINESE] = {"required": "Description is required"};
    json[this.QUANTITY] = {'required': 'Quantity is required'};
    json[this.POINT_SYSTEM] = {};
    json[this.PRICE_BEFORE_DISCOUNT] = {};
    return json;
  }

  successForm(object: ProductResponse) {
    if (!object) return;
    this.saveCategory(object.id);
    this.saveAttribute(object.id);
    this._routerService.navigate([PRODUCT_URL.ROOT, object.id]);

  }

  addAttributeProduct(event) {
    this.attributeProductModels.push(event)
  }

  removeAttribute(event){
   this.attributeProductModels.splice(this.attributeProductModels.indexOf(event), 1);
  }


  addCategoryProduct(event) {
    this.categoryProductModels.push(event);
  }

  removeCategory(event){
   this.categoryProductModels.splice(this.categoryProductModels.indexOf(event), 1);
  }

  saveCategory(productId: string) {
    for (let c of this.categoryProductModels) {
      this._productService.addProductCategory(productId, c.id, this._authService.getToken())
        .subscribe((response) => {
          if (response.status == '1') this.categoryProductModels = []
        });
    }
  }


  saveAttribute(productId: string) {
    for (let a of this.attributeProductModels) {
      this._productService.addProductAttribute(productId, a.attributeId, a.attributeValue, this._authService.getToken())
        .subscribe((response) => {
          if (response.status == '1') this.attributeProductModels = [];
        });
    }
  }


}














