import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductStockFormComponent} from './product-stock-form.component';

describe('ProductStockFormComponent', () => {
  let component: ProductStockFormComponent;
  let fixture: ComponentFixture<ProductStockFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductStockFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductStockFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
