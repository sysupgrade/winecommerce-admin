import {CategoryModel} from '../category/category.model';
import { AttributeProductModel} from '../attribute/attribute.model';

export class ProductModel {
  id: string;
  name: string;
  defaultPrice: string;
  description: string;
  status: string;
  createdAt: string;
  priceBeforeDiscount : string;
  updatedAt: string;
  alcoholPercentage : number;
  volumeInMl : number;
  quantity: string;
  categories: CategoryModel[] = [];
  attributes: AttributeProductModel[] = [];
  images : string[]=[];
  pointSystem : number;
  nameChinese : string;
  descriptionChinese:string;
}

export class ProductResponse {
  model: ProductModel;
  models: ProductModel[];
  total: number;
  status: string;
  id: string;
}


export class PRODUCT_URL {
  public static ROOT = '/pages/product/';
}

export class FilterProductModel {
  q: string;
  status: string;
  startDate: string;
  endDate: string;
}

export class ProductImageModel{
  imageId;
  productId;
  total;
}
