import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AttributeModel, AttributeResponse, AttributeProductModel} from '../../attribute/attribute.model';
import {AppSettings} from '../../app.setting';

@Component({
  selector: 'th-product-attribute-form',
  templateUrl: './product-attribute-form.component.html',
  styleUrls: ['./product-attribute-form.component.scss']
})
export class ProductAttributeFormComponent implements OnInit {

  @Input()
  model: AttributeModel = new AttributeModel();

  url: string = AppSettings.ATTRIBUTE_LIST + '?page=1&pageSize=10&q=:q';

  @Output()
  add: EventEmitter<AttributeProductModel> = new EventEmitter<AttributeProductModel>();

  models: AttributeModel[] = [];

  total: number = 0;

  constructor() {
  }

  ngOnInit() {
  }

  handleDataChange(response: AttributeResponse) {
    this.models = response.models;
    this.total = response.total;
  }

  addAttribute(attributeId: string, attributeName : string, attributeValue: string) {
    if(!attributeId || !attributeName || !attributeValue) {
      return;
    }
    this.add.emit({
      attributeId: attributeId,
      attributeName : attributeName,
      attributeValue: attributeValue,
      createdAt: null,
      updatedAt: null,
      status: null
    })
  }
}
