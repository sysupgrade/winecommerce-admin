import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppSettings} from '../../app.setting';
import {AttributeModel, AttributeResponse} from '../../attribute/attribute.model';
import {CategoryModel} from '../../category/category.model';

@Component({
  selector: 'th-product-category-form',
  templateUrl: './product-category-form.component.html',
  styleUrls: ['./product-category-form.component.scss']
})
export class ProductCategoryFormComponent implements OnInit {

  @Input()
  model: AttributeModel = new AttributeModel();

  url: string = AppSettings.CATEGORY_LIST + '?page=1&pageSize=10&q=:q';

  @Output()
  add: EventEmitter<CategoryModel> = new EventEmitter<CategoryModel>();

  models: AttributeModel[] = [];

  total: number = 0;

  constructor() {
  }

  ngOnInit() {
  }

  handleDataChange(response: AttributeResponse) {
    this.models = response.models;
    this.total = response.total;
  }

  addAttribute(val) {
    this.add.emit({
      id: val.value,
      name: val.text,
      description: null,
      createdAt: null,
      updatedAt: null
    })

  }
}
