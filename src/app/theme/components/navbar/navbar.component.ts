import {ApiUser} from './../../../user/user.model';
import {Router} from '@angular/router';
import {AuthService} from './../../../auth.service';
import {Component, ViewEncapsulation} from '@angular/core';
import {AppState} from '../../../app.state';
import {SidebarService} from '../sidebar/sidebar.service';

@Component({
  selector: 'az-navbar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [SidebarService]
})
export class NavbarComponent {
  public isMenuCollapsed: boolean = false;

  userName: string = localStorage.getItem(ApiUser.FIRST_NAME_STORAGE) + ' ' + localStorage.getItem(ApiUser.LAST_NAME_STORAGE);

  constructor(
    private _state: AppState,
    private _sidebarService: SidebarService,
    private _authService: AuthService,
    private _router: Router
  ) {
    this._state.subscribe('menu.isCollapsed', isCollapsed => {
      this.isMenuCollapsed = isCollapsed;
    });
  }

  public closeSubMenus() {
    /* when using <az-sidebar> instead of <az-menu> uncomment this line */
    // this._sidebarService.closeAllSubMenus();
  }

  public toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
  }

  logOut() {
    this._authService.logout();
    this._router.navigateByUrl('/login');
  }
}
