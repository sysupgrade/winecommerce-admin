export const menuItems = [
  // {
  //   title: 'Dashboard',
  //   routerLink: 'dashboard',
  //   icon: 'fa-home',
  //   selected: false,
  //   expanded: false,
  //   order: 0
  // },
  {
    title: 'Product',
    routerLink: 'product',
    icon: 'fa-newspaper-o',
    selected: false,
    expanded: false,
    order: 25,
    subMenu: [
      {
        title: 'Add',
        routerLink: 'product/add',
        icon: 'fa-plus'
      },
      {
        title: 'List',
        routerLink: 'product',
        icon: 'fa-list'
      },
      {
        title: 'Attribute',
        routerLink: 'attribute',
        icon: 'fa-list'
      },
      {
        title: "Featured Attribute",
        routerLink: "attribute/featured",
        icon: "fa-list"
      },{
        title: 'Category',
        routerLink: 'category',
        icon: 'fa-list'
      },
    ]
  },
  {
    title: 'Sales Management',
    routerLink: 'sales',
    icon: 'fa-usd',
    selected: false,
    expanded: false,
    order: 300,
    subMenu: [
      {
        title: 'Order',
        routerLink: 'sales',
        icon: 'fa-list'
      },
      {
        title: 'Invoice',
        routerLink: 'invoice',
        icon: 'fa-list'
      },
      {
        title: 'Coupon',
        routerLink: 'salesrule',
        icon: 'fa-list'
      },
      {
        title: 'Shipment',
        routerLink: 'shipment',
        icon: 'fa-list'
      }
    ]
  },
  {
    title: 'Content Management',
    routerLink: 'blog',
    icon: 'fa-newspaper-o',
    selected: false,
    expanded: false,
    order: 80,
    subMenu: [
      {
        title: 'News',
        routerLink: 'news',
        icon: 'fa-plus'
      },
      {
        title: 'Newsletter',
        routerLink: 'newsletter',
        icon: 'fa-list'
      },
      // {
      //   title: 'Feedback',
      //   routerLink: 'feedback',
      //   icon: 'fa-list'
      // },
      {
        title: 'Testimonial',
        routerLink: 'testimonial'
      },
      {
        title: 'Event',
        routerLink: 'event'
      },
      {
        title: 'Contact',
        routerLink: 'contact'
      }
    ]
  },
  {
    title: 'User',
    routerLink: 'user',
    icon: 'fa-user',
    order: 400,
    subMenu: [
      {
        title: 'List User',
        routerLink: 'user',
        icon: 'fa-plus'
      },
      {
        title: 'Subscription',
        routerLink: 'subscription'
      }
    ]
  },
  {
    title: 'Report Management',
    routerLink: 'report/order',
    icon: 'fa-print',
    selected: false,
    expanded: false,
    order: 300
  },
];
